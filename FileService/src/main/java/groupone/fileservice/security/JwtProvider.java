package groupone.fileservice.security;

import groupone.fileservice.exception.NoTokenException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@PropertySource("classpath:application.properties")
public class JwtProvider {
    @Value("${security.jwt.token.key}")
    private String key;

    // resolves the token -> use the information in the token to create a userDetail object
    public Optional<AuthUserDetail> resolveToken(HttpServletRequest request) throws NoTokenException {
        String prefixedToken = request.getHeader("Authorization"); // extract token value by key "Authorization"
        String token = null;
        if(prefixedToken != null && prefixedToken.startsWith("Bearer")) {
            token = prefixedToken.substring(7); // remove the prefix "Bearer "
        } else {
            return Optional.empty();
        }
        System.out.println("Token:" + token);
        Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody(); // decode

        int userId = (int)claims.get("userId");
        List<GrantedAuthority> authorities = ((List<String>)claims.get("permissions")).stream()
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        System.out.println(authorities);

        //return a userDetail object with the permissions the user has
        return Optional.of(AuthUserDetail.builder()
                .userId(userId)
                .authorities(authorities)
                .build());
    }
}
