package groupone.fileservice.service;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

@Service
@Slf4j
public class FileService {

    @Autowired
    private AmazonS3 s3;

    private String bucketName = "bfgroupone";

    public String uploadFile(MultipartFile file) {
        File fileObj = convertMultiPartFileToFile(file);
        String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();
        try {
            s3.putObject(new PutObjectRequest(bucketName, fileName, fileObj));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fileObj.delete();  // delete the local file after upload
        }
        return fileName;
    }

    public String uploadFileURL(MultipartFile file) {
        File fileObj = convertMultiPartFileToFile(file);
        CannedAccessControlList acl = CannedAccessControlList.PublicRead;
        String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();
        s3.putObject(new PutObjectRequest(bucketName, fileName, fileObj).withCannedAcl(acl));
        fileObj.delete();
        return s3.getUrl(bucketName, fileName).toString();
    }

//    public byte[] downloadFile(String fileName) {
//        S3Object s3Object = s3.getObject(bucketName, fileName);
//        S3ObjectInputStream inputStream = s3Object.getObjectContent();
//        try {
//            byte[] content = IOUtils.toByteArray(inputStream);
//            return content;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public String downloadFile(String fileName) {
        // Set the presigned URL to expire after one hour.
        Date expiration = new java.util.Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * 60 * 60;  // 1 hour
        expiration.setTime(expTimeMillis);

        // Generate the presigned URL.
        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(bucketName, fileName)
                        .withMethod(HttpMethod.GET)
                        .withExpiration(expiration);
        return s3.generatePresignedUrl(generatePresignedUrlRequest).toString();
    }

    public String deleteFile(String fileName) {
        s3.deleteObject(bucketName, fileName);
        return fileName + " remove...";
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error converting multipartFile to file", e);
        }
        return convertedFile;
    }
}
