package groupone.fileservice.config;

import groupone.fileservice.security.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private JwtFilter jwtFilter;

    @Autowired
    public void setJwtFilter(JwtFilter jwtFilter) {
        this.jwtFilter = jwtFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().and()
                .authorizeRequests()
                .antMatchers("/file-service/upload").permitAll()
                .antMatchers("/file-service/download/*").permitAll()
                .antMatchers("/file-service/delete/*").permitAll()
//                .antMatchers("/register").permitAll()
//                .antMatchers(HttpMethod.GET,"/users").hasAuthority("user_read")
//                .antMatchers(HttpMethod.PATCH,"/products/*").hasAuthority("product_update")
//                .antMatchers(HttpMethod.POST,"/products").hasAuthority("product_update")
                .anyRequest()
                .authenticated().and().addFilterAfter(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
