package groupone.messageservice.security;

import groupone.messageservice.exception.NoTokenException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class JwtFilter extends OncePerRequestFilter {
    private JwtProvider jwtProvider;

    @Autowired
    public void setJwtProvider(JwtProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {
        Optional<AuthUserDetail> authUserDetailOptional = null; // extract jwt from request, generate a userdetails object
        System.out.println("request url: " + request.getRequestURI());
        try {
            authUserDetailOptional = jwtProvider.resolveToken(request);
        } catch (NoTokenException e) {
            throw new RuntimeException(e);
        }

        if (authUserDetailOptional.isPresent()){
            AuthUserDetail authUserDetail = authUserDetailOptional.get();
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                    authUserDetail.getUsername(),
                    authUserDetail.getUserId(),
                    authUserDetail.getAuthorities()
            ); // generate authentication object

            SecurityContextHolder.getContext().setAuthentication(authentication); // put authentication object in the secruitycontext
        }
        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        try {
            System.out.println("in should not filter before resolve token");
            Optional<AuthUserDetail> auth = jwtProvider.resolveToken(request);
            return !auth.isPresent();
        } catch (NoTokenException e){
            System.out.println("should not filter path: " + request.getRequestURI());
            SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
            return "/message-service/messages".equals(request.getRequestURI());
        } catch (MalformedJwtException e){
            System.out.println("should not filter path: " + request.getRequestURI());
            SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
            return "/message-service/messages".equals(request.getRequestURI());
        }
    }
}
