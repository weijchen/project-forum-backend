package groupone.messageservice.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Message")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    public static final String STATUS_OPEN = "Open";
    public static final String STATUS_CLOSE = "Close";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "messageId")
    private int messageId;

    @Column(name = "userId")
    private int userId;

    @Column(name = "subject")
    private String subject;

    @Column(name = "email")
    private String email;

    @Column(name = "message")
    private String message;

    @Column(name = "dateCreated")
    @Temporal(TemporalType.DATE)
    private Date dateCreated;

    @Column(name = "status")
    private String status;
}
