package groupone.messageservice.exception;

public class NoTokenException extends Exception{
    public NoTokenException(String s) {super(s);}
}
