package groupone.messageservice.controller;

import groupone.messageservice.dto.DataResponse;
import groupone.messageservice.dto.MessageRequest;
import groupone.messageservice.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/messages")
public class MessageController {
    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }


    @GetMapping("/all")
    public ResponseEntity<DataResponse> getAllMessages(){
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(messageService.getAllMessages())
                .message("Successfully get all messages")
                .build(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<DataResponse> createMessage(@RequestBody MessageRequest messageRequest) {
        int userId = 0;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            userId =(Integer) authentication.getCredentials();
        }

        messageService.addMessage(messageRequest, userId);
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .message("Message created")
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{messageId}/status")
    public ResponseEntity<DataResponse> updateMessageStatus(@PathVariable int messageId) {
        messageService.updateMessageStatus(messageId);
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .message("Message status updated")
                .build(), HttpStatus.OK);
    }
}
