package groupone.messageservice.dao;

import groupone.messageservice.entity.Message;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class MessageDao extends AbstractHibernateDao<Message> {
    public MessageDao() {
        setClazz(Message.class);
    }

    public Message getMessageById(int id) {
        return this.findById(id);
    }

    public List<Message> getAllMessages() {
        return this.getAll();
    }

    public void addMessage(Message message) {
        this.add(message);
    }

    public void deleteMessage(Message message) {
        this.delete(message);
    }
}
