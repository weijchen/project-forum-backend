package groupone.messageservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class DataResponse {
    private Boolean success;
    private String message;
    private Object data;
}
