package groupone.messageservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class MessageRequest {
    String subject;
    String email;
    String message;
}
