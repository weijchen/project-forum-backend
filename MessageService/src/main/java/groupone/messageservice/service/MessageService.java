package groupone.messageservice.service;

import groupone.messageservice.dao.MessageDao;
import groupone.messageservice.dto.MessageRequest;
import groupone.messageservice.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MessageService {
    private final MessageDao messageDao;

    @Autowired
    public MessageService(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    public List<Message> getAllMessages() {
        return messageDao.getAllMessages();
    }

    public void addMessage(MessageRequest messageRequest, int userId) {
        Message message = Message.builder()
                .userId(userId)
                .subject(messageRequest.getSubject())
                .email(messageRequest.getEmail())
                .message(messageRequest.getMessage())
                .dateCreated(new Date())
                .status(Message.STATUS_OPEN).build();
        messageDao.addMessage(message);
    }

    public void updateMessageStatus(int messageId) {
        Message message = messageDao.getMessageById(messageId);
        String status  = message.getStatus();
        String newStatus = status.equals(Message.STATUS_OPEN) ? Message.STATUS_CLOSE : Message.STATUS_OPEN;
        message.setStatus(newStatus);
    }
}
