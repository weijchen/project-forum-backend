# project-forum-backend

A Reddit-like forum constructed under microservice architecture

## Project Features

### General
- Authentication (Login / Register) - JWT
- Authorization: separate pages based on the identity of login user
- RWD website built with React, Redux, and Bootstrap
- Connect with AWS RDS, S3, and MongoDB
- Application constructed under microservice architecture (Spring Cloud, Eureka, Gateway, Feign Client)
- Spring MVC structure
- RESTful API design
- Spring Security and Input data validation
- AMQP (RabbitMQ)
- Unit testing: JUnit and Mockito

### Admin User
- User Management
  - Promote
  - Ban
- Post Management
  - Ban
  - Delete

### Normal User
- Post
  - Create
  - Update (Content, Status, Hide, Archive, ...)
  - Delete
  - Reply & Subreply
- Profile
  - Update
  - Verification email
- History
  - Post history
  - Reply history
