package groupone.historyservice.aop;

import groupone.historyservice.dto.common.ErrorResponse;
import groupone.historyservice.exception.NoTokenException;
import groupone.historyservice.exception.TokenNotValidException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AOPExceptionHandler {
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        return new ResponseEntity<>(ErrorResponse.builder().message("Exception: " + e.getMessage()).build(), HttpStatus.OK);
    }

    @ExceptionHandler(TokenNotValidException.class)
    public ResponseEntity<ErrorResponse> handleTokenNotValidException(TokenNotValidException ex) {
        // Handle TokenNotValidException
        String errorMessage = "Token validation failed: " + ex.getMessage();
        return new ResponseEntity<>(ErrorResponse.builder().message(errorMessage).build(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(NoTokenException.class)
    public ResponseEntity<ErrorResponse> handleTokenNotValidException(NoTokenException ex) {
        // Handle TokenNotValidException
        String errorMessage = ex.getMessage();
        return new ResponseEntity<>(ErrorResponse.builder().message(errorMessage).build(), HttpStatus.UNAUTHORIZED);
    }
}
