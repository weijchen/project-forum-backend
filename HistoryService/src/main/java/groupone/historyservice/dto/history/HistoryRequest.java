package groupone.historyservice.dto.history;

import lombok.*;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HistoryRequest {
    private String postId;

}
