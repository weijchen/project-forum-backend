package groupone.historyservice.dto.history;

import lombok.*;

import javax.persistence.Column;
import java.sql.Timestamp;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HistoryResponse {
    private Integer history_id;
    private Integer userId;
    private String postId;
    private Timestamp viewDate;
}
