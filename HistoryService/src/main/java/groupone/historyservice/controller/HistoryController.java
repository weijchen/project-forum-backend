package groupone.historyservice.controller;

import groupone.historyservice.Security.JwtProvider;
import groupone.historyservice.dto.common.DataResponse;
import groupone.historyservice.dto.history.HistoryRequest;
import groupone.historyservice.dto.history.HistoryResponse;
import groupone.historyservice.entity.History;
import groupone.historyservice.exception.NoTokenException;
import groupone.historyservice.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
public class HistoryController {

    private HistoryService historyService;

    @Autowired
    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    private JwtProvider jwtProvider;

    @Autowired
    public void setJwtProvider(JwtProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }

    @PostMapping("/addOrUpdate")
    public ResponseEntity<DataResponse> addOrUpdateHistory(@RequestBody HistoryRequest historyRequest) throws NoTokenException {
        Integer userId = 0;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userId = (Integer) authentication.getCredentials();

        try {
            historyService.saveOrUpdate(historyRequest, userId);
            DataResponse response = DataResponse.builder()
                    .success(true)
                    .message("History added/updated")
                    .build();
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            DataResponse response = DataResponse.builder()
                    .success(false)
                    .message(e.getMessage())
                    .build();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }


    }

    @GetMapping("/all")
    public ResponseEntity<DataResponse> getAllHistory() throws NoTokenException {
//        System.out.println("Getting all history: request: " + request.toString());

        Integer userId = 0;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userId = (Integer) authentication.getCredentials();
        List<History> allHistory = historyService.getAllHistory(userId);
        List<HistoryResponse> historyResponses = new ArrayList<>();
        for (History history : allHistory) {
            HistoryResponse historyResponse = HistoryResponse.builder()
                    .history_id(history.getId())
                    .postId(history.getPostId())
                    .userId(history.getUserId())
                    .viewDate(history.getViewDate())
                    .build();
            historyResponses.add(historyResponse);
        }
        DataResponse response = DataResponse.builder()
                .success(true)
                .message("Get all history success")
                .data(historyResponses)
                .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/allHistory")
    public ResponseEntity<DataResponse> getAllHistoryPostIds() throws NoTokenException {
        List<History> allHistory = historyService.getAllHistoryPostIds();
        List<String> postIds = new ArrayList<>();
        for(History history : allHistory){
            postIds.add(history.getPostId());
        }
        DataResponse response = DataResponse.builder()
                .success(true)
                .message("All history from all user sort by view dates")
                .data(postIds)
                .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/allPostId")
    public ResponseEntity<DataResponse> getAllPostIdOfHistory() throws NoTokenException {
//        System.out.println("Getting all history: request: " + request.toString());

        Integer userId = 0;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userId = (Integer) authentication.getCredentials();
        List<History> allHistory = historyService.getAllHistory(userId);
        List<String> postIds = new ArrayList<>();
        for (History history : allHistory) {
            postIds.add(history.getPostId());
        }
        DataResponse response = DataResponse.builder()
                .success(true)
                .message("All history post ids for current user sort by view dates")
                .data(postIds)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/historyByDate")
    public ResponseEntity<DataResponse> findHistoryByDate(@RequestParam("date") String date) throws NoTokenException {
        LocalDate dateParsed = LocalDate.parse(date);
        Integer userId = 0;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userId = (Integer) authentication.getCredentials();
        List<History> allHistory = historyService.getHistoryByDate(dateParsed, userId);
        DataResponse response = DataResponse.builder()
                .success(true)
                .message("All history for current user within specific date")
                .data(allHistory)
                .build();
        return ResponseEntity.ok(response);
    }
}
