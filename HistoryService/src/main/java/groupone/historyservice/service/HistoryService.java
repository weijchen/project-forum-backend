package groupone.historyservice.service;

import groupone.historyservice.dao.HistoryDao;
import groupone.historyservice.dto.history.HistoryRequest;
import groupone.historyservice.entity.History;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class HistoryService {

    private  HistoryDao historyDao;


    @Autowired
    public void setHistoryDao(HistoryDao historyDao) {
        this.historyDao=historyDao;
    }

    @Transactional
    public List<History> getAllHistory(Integer userId){
        List<History> histories = historyDao.getAllHistorySortedByViewDate(userId);
//        for(History history : histories){
//            System.out.println(history);
//        }
        return histories;
    }
    @Transactional
    public List<History> getAllHistoryPostIds(){
        List<History> histories = historyDao.getAllHistoryPostIds();
        return histories;
    }

    @Transactional
    public void saveOrUpdate(HistoryRequest historyRequest, Integer userId){
        historyDao.addOrUpdateHistory(historyRequest, userId);
    }

    @Transactional
    public List<History> getHistoryByDate(LocalDate date, Integer userId){
        List<History> histories = historyDao.getAllHistorySortedByViewDate(userId);
        List<History> result = new ArrayList<>();
        for(History history : histories){

            LocalDateTime localDateTime = history.getViewDate().toLocalDateTime();
            LocalDate localDate = localDateTime.toLocalDate();
//            System.out.println(history.getViewDate() + " " +  localDateTime + " " + localDate);
            if(date.equals(localDate)){
                result.add(history);

            }
        }
        return result;
    }
}
