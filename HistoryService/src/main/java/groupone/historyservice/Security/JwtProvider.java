package groupone.historyservice.Security;


import groupone.historyservice.exception.NoTokenException;
import groupone.historyservice.exception.TokenNotValidException;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Component
@PropertySource("classpath:application.properties")
public class JwtProvider {
    @Value("${security.jwt.token.key}")
    private String key;

    public String extractToken(HttpServletRequest request) throws NoTokenException {

        String header = request.getHeader("Authorization");
        if(header == null) {
            throw new NoTokenException("No token founded, please login first.");
        }
        if (header != null && header.startsWith("Bearer ")) {
            return header.substring(7); // Extract the token without the "Bearer " prefix
        }
        return null; // Token not found in the request header
    }

    public Optional<AuthUserDetail> resolveToken(HttpServletRequest request) throws TokenNotValidException, NoTokenException {
        String token = extractToken(request);
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token)
                    .getBody();// decode
            System.out.println("resolveToken: Claims: " + claims);
            String email = claims.getSubject();
            Integer userId = (Integer) claims.get("userId");
            List<GrantedAuthority> authorities = ((List<String>) claims.get("permissions")).stream()
                    .map(SimpleGrantedAuthority::new).collect(Collectors.toList());
            // Fetch user details from the database based on the username
            System.out.println("In resolve Token, authorities are: ");
            for(GrantedAuthority authority: authorities){
                System.out.println(authority);
            }
            return Optional.of(AuthUserDetail.builder()
                    .username(email)
                    .email(email)
                    .userId(userId)
                    .authorities(authorities)
                    .build());
        } catch (TokenNotValidException e){
            System.out.println("Token is not valid! " + e);
            return null;
        }
    }


}