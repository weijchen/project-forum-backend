package groupone.historyservice.config;

//import groupone.historyservice.Security.JwtAuthenticationEntryPoint;
//import groupone.historyservice.Security.JwtAuthenticationProvider;
import groupone.historyservice.Security.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {



//    private final JwtAuthenticationProvider jwtAuthenticationProvider;
//    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
//
//    @Autowired
//    public SecurityConfig(JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint) {
//        this.jwtAuthenticationProvider = jwtAuthenticationProvider;
//        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
//    }
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(jwtAuthenticationProvider);
//    }
    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
    private JwtFilter jwtFilter;

    @Autowired
    public void setJwtFilter(JwtFilter jwtFilter) {
        this.jwtFilter = jwtFilter;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().and()

                .authorizeRequests()
                .antMatchers("/history-service/**").hasAuthority("read")
                .anyRequest()
                .authenticated().and().addFilterAfter(jwtFilter, UsernamePasswordAuthenticationFilter.class);
//
    }
}
