package groupone.historyservice.dao;

import groupone.historyservice.dto.history.HistoryRequest;
import groupone.historyservice.entity.History;
import org.hibernate.Session;
import org.hibernate.query.Query;

import org.springframework.stereotype.Repository;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Repository
public class HistoryDao extends AbstractHibernateDao<History>{

    public HistoryDao(){
        setClazz(History.class);
    }

    public History getHistoryById(Integer id){
        return this.findById(id);
    }

    public List<History> getAllHistory(){
        return this.getAll();
    }


    public List<History> getAllHistoryPostIds(){
        CriteriaBuilder cb = this.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<History> query = cb.createQuery(clazz);
        Root<History> historyRoot = query.from(clazz);
        query.select(historyRoot)
                .orderBy(cb.desc(historyRoot.get("viewDate")));

        return this.getCurrentSession().createQuery(query).getResultList();
    }
    public List<History> getAllHistorySortedByViewDate(Integer userId) {
        CriteriaBuilder cb = this.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<History> query = cb.createQuery(clazz);
        Root<History> historyRoot = query.from(clazz);
        query.select(historyRoot)
                .where(cb.equal(historyRoot.get("userId"), userId))
                .orderBy(cb.desc(historyRoot.get("viewDate")));

        return this.getCurrentSession().createQuery(query).getResultList();
    }

    public void addOrUpdateHistory(HistoryRequest history, Integer userId) {
        Session session = this.getCurrentSession();

        // Check if the entry exists in the history table
        String selectQuery = "SELECT h FROM History h WHERE h.userId = :userId AND h.postId = :postId";
        History existingEntry = session.createQuery(selectQuery, History.class)
                .setParameter("userId", userId)
                .setParameter("postId", history.getPostId())
                .uniqueResult();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        if (existingEntry != null) {
            // Entry exists, update it with the new data
            existingEntry.setViewDate(timestamp);
            // Update any other properties as needed
            session.merge(existingEntry);
        } else {
            // Entry doesn't exist, insert a new entry
            History newHistory = new History();
            newHistory.setPostId(history.getPostId());
            newHistory.setUserId(userId);
            newHistory.setViewDate(timestamp);
            this.add(newHistory);
        }

    }
    public void addHistory(History history){
        this.add(history);
    }

    public void deleteHistory(History history){
        this.delete(history);
    }

//    @PersistenceContext
//    private EntityManager entityManager;
//
//    public List<History> getAllHistory() {
//        String query = "SELECT h FROM History h";
//        return entityManager.createQuery(query, History.class).getResultList();
//    }
//
//    public void addHistory(History history) {
//        entityManager.persist(history);
//    }
}
