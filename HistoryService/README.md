# HistoryService
Get all histories for current user, must have correct JWT token in the request
API: http://localhost:8083/history-service/all

Add or Update history
API: http://localhost:8083/history-service/addOrUpdate
Body raw JSON example:
{
    "postId": 1
}
