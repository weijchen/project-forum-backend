package com.example.compositeservice.service;

import com.example.compositeservice.dto.common.DataResponse;
import com.example.compositeservice.dto.common.MessageResponse;
import com.example.compositeservice.dto.history.HistoryResponse;
import com.example.compositeservice.dto.post.*;
import com.example.compositeservice.dto.user.*;
import com.example.compositeservice.entity.*;
import com.example.compositeservice.exception.*;
import com.example.compositeservice.security.JwtContext;
import com.example.compositeservice.service.remote.RemoteFileService;
import com.example.compositeservice.service.remote.RemoteHistoryService;
import com.example.compositeservice.service.remote.RemotePostService;
import com.example.compositeservice.service.remote.RemoteUserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class CompositeService {
    private RemoteFileService remoteFileService;
    private RemotePostService remotePostService;
    private RemoteHistoryService remoteHistoryService;
    private CacheManager cacheManager;
    private RemoteUserService remoteUserService;
    private final String token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlbWFpbDRAZW1haWwuY29tIiwicGVybWlzc2lvbnMiOlsicmVhZCJdLCJ1c2VySWQiOjQsImVtYWlsIjoiZW1haWw0QGVtYWlsLmNvbSIsImFjdGl2ZSI6dHJ1ZSwicm9sZSI6Ik5PUk1BTF9VU0VSX05PVF9WQUxJRCJ9.1Is8gRUvItZ6krQVw5mkCccHy1R2sACL6lTzqzvoZNU";

    private String cachePostId = "";
    @Autowired
    public CompositeService(RemoteFileService remoteFileService, RemotePostService remotePostService,
                            RemoteHistoryService remoteHistoryService, CacheManager cacheManager) {
        this.remoteFileService = remoteFileService;
        this.remotePostService = remotePostService;
        this.remoteHistoryService = remoteHistoryService;
        this.cacheManager = cacheManager;
    }

    @Autowired
    public void setRemoteUserService(RemoteUserService remoteUserService) {this.remoteUserService = remoteUserService;}


//    @EventListener(ApplicationStartedEvent.class )
//    public void listenToStart(ApplicationStartedEvent event) {
//        System.out.println("here you go");
//        executorService.submit(() -> {
//                    JwtContext.setJwtToken("Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlbWFpbDRAZW1haWwuY29tIiwicGVybWlzc2lvbnMiOlsicmVhZCJdLCJ1c2VySWQiOjQsImVtYWlsIjoiZW1haWw0QGVtYWlsLmNvbSIsImFjdGl2ZSI6dHJ1ZSwicm9sZSI6Ik5PUk1BTF9VU0VSX05PVF9WQUxJRCJ9.1Is8gRUvItZ6krQVw5mkCccHy1R2sACL6lTzqzvoZNU");
//                    getOrUpdateCache();
//                    JwtContext.clear();
//                });
//    }

    @Scheduled(fixedRate = 10 * 60 * 1000)
    public void updateCache() {
        cacheManager.getCache("posts").clear();
        boolean setToken = false;
        if(JwtContext.getJwtToken() == null) {
            JwtContext.setJwtToken(token);
            setToken = true;
        }
        getOrUpdateCache();
        if(setToken) JwtContext.clear();
    }

    public void updateCache(String postId) {
        if(postId.equals(cachePostId)) {
            cacheManager.getCache("posts").clear();
            getOrUpdateCache();
        }
    }

    public void getOrUpdateCache() {
        try {
            DataResponse dataResponse = remoteHistoryService.getAllHistoryPostIds().getBody();
            List<String> postIds = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();

            if(dataResponse != null && dataResponse.getData() != null) {
                try {
                    String jsonData = objectMapper.writeValueAsString(dataResponse.getData());
                    postIds = objectMapper.readValue(jsonData, new TypeReference<List<String>>(){});
                    if(postIds.size() > 5) postIds = postIds.subList(0, 5);
                } catch (JsonProcessingException e) {
                    throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
                }
                if(postIds.size() == 0) {
                    System.out.println("nothing");
                    return;
                }
                HashMap<String, Integer> countMap = new HashMap<>();
                for(String id : postIds) {
                    ResponseEntity<DataResponse> response = remotePostService.getAPostStatus(id);
                    if(response.getBody().getSuccess()) {
                        String status = response.getBody().getData().toString();
                        if(status.equals("Published")) {
                            countMap.put(id, countMap.getOrDefault(id, 0) + 1);
                        }
                    }
                }

                List<String> top1 = countMap.entrySet().stream()
                        .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                        .limit(1).map(Map.Entry::getKey).collect(Collectors.toList());
                System.out.println("Top1: " + top1);

                PostDTO post = getPostById(top1.get(0));
                if(post != null) {
                    cacheManager.getCache("posts").put(top1.get(0), post);
                    cachePostId = top1.get(0);
                    System.out.println("cachePostId: " + cachePostId);
                }
            }
        } catch (Exception e) {
            System.out.println("something's wrong");
        }
    }

    public List<Post> getAllPublishedPosts() {
        ResponseEntity<DataResponse> response = remotePostService.getAllPublishedPosts();
        if (response.getBody().getSuccess()) {
            DataResponse dataResponse = remotePostService.getAllPublishedPosts().getBody();
            return convertResponseAndGetImagesUrl(dataResponse);
        } else {
            throw new PostServerException(response.getBody().getMessage());
        }
    }

    public ResponseEntity<String> uploadFile(MultipartFile file) {
        return this.remoteFileService.uploadFile(file);
    }
    public ResponseEntity<String> uploadUrlFile(MultipartFile file) {
        return this.remoteFileService.uploadUrlFile(file);
    }

    public MessageResponse createNewPost(InitialPostRequest request, MultipartFile[] images, MultipartFile[] files) {
        PostStatus status = request.getStatus();
        if(request.getTitle() == null || request.getTitle().trim().length() == 0) {
            throw new EntryNotCorrectException("Title is required");
        }

        if (status != PostStatus.Published && status != PostStatus.Unpublished)
            throw new StatusNotCorrectException("Status is not correct, try again");

        List<String> imagesList = new ArrayList<>();
        List<String> filesList = new ArrayList<>();
        if (images != null && images.length != 0) {
            List<CompletableFuture<String>> imagesFutures = new ArrayList<>();
            for (MultipartFile file : images) {
                System.out.println("file send to file system:" + file);
                imagesFutures.add(uploadFileAsync(file));
            }
            imagesList = imagesFutures.stream().map(CompletableFuture::join).collect(Collectors.toList());
        }

        if (files != null && files.length != 0) {
            List<CompletableFuture<String>> filesFutures = new ArrayList<>();
            for (MultipartFile file : files) {
                filesFutures.add(uploadFileAsync(file));
            }
            filesList = filesFutures.stream().map(CompletableFuture::join).collect(Collectors.toList());
        }

        CreatePostRequest createPostRequest = CreatePostRequest.builder()
                .title(request.getTitle())
                .content(request.getContent())
                .status(request.getStatus())
                .images(imagesList)
                .attachments(filesList)
                .build();
        ResponseEntity<MessageResponse> response = remotePostService.createPost(createPostRequest);
        if (response.getBody().getSuccess()) {
            return response.getBody();
        } else {
            throw new PostServerException("Exception occur when try to call createPost() in post service");
        }
    }

    @Async
    public CompletableFuture<String> uploadFileAsync(MultipartFile file) {
        return CompletableFuture.completedFuture(remoteFileService.uploadFile(file).getBody());
    }

    public PostDTO getPostById(String id) {
        Cache cache = cacheManager.getCache("posts");
        if(cache != null && cache.get(id) != null) {
            return (PostDTO) cache.get(id).get();
        }
        ResponseEntity<DataResponse> response = remotePostService.getPost(id);
        if (response.getBody().getSuccess()) {
            DataResponse dataResponse = response.getBody();
            ObjectMapper objectMapper = new ObjectMapper();
            Post post = null;
            if (dataResponse.getSuccess() && dataResponse.getData() != null) {
                try {
                    String jsonData = objectMapper.writeValueAsString(dataResponse.getData());
                    post = objectMapper.readValue(jsonData, new TypeReference<Post>() {});
                } catch (JsonProcessingException e) {
                    throw new ParsingModelException("JsonProcessingException occurred during parsing the response in post");
                }
            }
            if (post != null && post.getImages() != null && post.getImages().size() > 0) {
                post.setImages(post.getImages().stream()
                        .map(image -> remoteFileService.downloadFile(image).getBody()).collect(Collectors.toList()));
            }
            if(post != null) {
                HashSet<Integer> set = new HashSet<>();
                set.add(post.getUserId());
                for(PostReply reply : post.getPostReplies()) {
                    set.add(reply.getUserId());
                    for(SubReply subReply : reply.getSubReplies()) {
                        set.add(subReply.getUserId());
                    }
                }
                List<Integer> userIds = new ArrayList<>(set);
                GeneralInfoRequest generalInfoRequest = new GeneralInfoRequest(userIds);
                DataResponse response1 = remoteUserService.getUsersByIdsGeneral(generalInfoRequest).getBody();
                ObjectMapper objectMapper1 = new ObjectMapper();
                List<UserGeneralDTO> userGeneralDTO = null;
                if(response1 != null && response1.getSuccess() && response1.getData() != null) {
                    try {
                        String jsonData1 = objectMapper1.writeValueAsString(response1.getData());
                        userGeneralDTO = objectMapper.readValue(jsonData1, new TypeReference<List<UserGeneralDTO>>() {
                        });
                    } catch (JsonProcessingException e) {
                        throw new ParsingModelException("JsonProcessingException occurred during parsing the response in user");
                    }
                }
                HashMap<Integer, UserGeneralDTO> map = new HashMap<>();
                for(int i = 0; i < set.size(); i++) {
                    map.put(userIds.get(i), userGeneralDTO.get(i));
                }
                UserGeneralDTO currentUser = map.get(post.getUserId());
                return PostDTO.builder().postId(post.getPostId()).userId(post.getUserId())
                        .firstName(currentUser.getFirstName()).lastName(currentUser.getLastName())
                        .profileImageURL(currentUser.getProfileImageURL()).title(post.getTitle())
                        .content(post.getContent()).dateCreated(post.getDateCreated())
                        .isArchived(post.getIsArchived())
                        .status(post.getStatus())
                        .dateModified(post.getDateModified())
                        .images(post.getImages()).attachments(post.getAttachments())
                        .postReplies(post.getPostReplies().stream().map(reply -> {
                            UserGeneralDTO replyUser = map.get(reply.getUserId());
                            return ReplyDTO.builder().userId(reply.getUserId())
                                    .firstName(replyUser.getFirstName()).lastName(replyUser.getLastName())
                                    .profileImageURL(replyUser.getProfileImageURL())
                                    .comment(reply.getComment()).dateCreated(reply.getDateCreated())
                                    .isActive(reply.isActive())
                                    .subReplies(reply.getSubReplies().stream().map(subReply -> {
                                                UserGeneralDTO subreplyUser = getUserGeneralInfo(subReply.getUserId());
                                                return SubreplyDTO.builder().userId(subReply.getUserId())
                                                        .firstName(subreplyUser.getFirstName())
                                                        .lastName(subreplyUser.getLastName())
                                                        .profileImageURL(subreplyUser.getProfileImageURL())
                                                        .comment(subReply.getComment())
                                                        .isActive(subReply.isActive())
                                                        .dateCreated(subReply.getDateCreated()).build();
                                            })
                                            .collect(Collectors.toList())).build();
                        }).collect(Collectors.toList())).build();
            } else {
                throw new PostNotExistException("The post does not exist");
            }
        } else {
            throw new PostServerException(response.getBody().getMessage());
        }
    }

    public UserGeneralDTO getUserGeneralInfo(int userId) {
        DataResponse response = remoteUserService.getUserByIdGeneral(userId).getBody();
        ObjectMapper objectMapper = new ObjectMapper();
        UserGeneralDTO user = null;
        if (response == null) {
            throw new UserNotExistException("The user is not exist");
        }
        if(response.getSuccess() != null && response.getSuccess() && response.getData() != null) {
            try {
                String jsonData = objectMapper.writeValueAsString(response.getData());
                user = objectMapper.readValue(jsonData, new TypeReference<UserGeneralDTO>() {});
            } catch (JsonProcessingException e) {
                throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
            }
            return user;
        } else {
            throw new UserNotExistException("The user is not exist");
        }
    }

    public List<Post> getAllUnpublishedPosts() {
        ResponseEntity<DataResponse> response = remotePostService.getAllUnpublishedPosts();
        if (response.getBody().getSuccess()) {
            DataResponse dataResponse = response.getBody();
            return convertResponseAndGetImagesUrl(dataResponse);
        } else {
            throw new PostServerException(response.getBody().getMessage());
        }
    }

    public List<Post> getUserTop3ReplyPosts() {
        ResponseEntity<DataResponse> response = remotePostService.getUserTop3ReplyPosts();
        if (response.getBody().getSuccess()) {
            DataResponse dataResponse = response.getBody();
            return convertResponseAndGetImagesUrl(dataResponse);
        } else {
            throw new PostServerException(response.getBody().getMessage());
        }
    }

    private List<Post> convertResponseAndGetImagesUrl(DataResponse dataResponse) {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Post> posts = new ArrayList<>();

        if (dataResponse.getSuccess() && dataResponse.getData() != null) {
            try {
                String jsonData = objectMapper.writeValueAsString(dataResponse.getData());
                posts = objectMapper.readValue(jsonData, new TypeReference<List<Post>>() {
                });
            } catch (JsonProcessingException e) {
                throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
            }
        }
        if (posts != null && posts.size() > 0) {
            return posts.stream().peek(post -> {
                        if (post.getImages() != null && post.getImages().size() > 0)
                            post.setImages(post.getImages().stream()
                                    .map(image -> remoteFileService.downloadFile(image).getBody()).collect(Collectors.toList()));
                    })
                    .collect(Collectors.toList());
        }
        return posts;
    }

    public ResponseEntity<AllPostInHistoryResponse> getAllPostInHistory() {
        ResponseEntity<DataResponse> historyResponse = remoteHistoryService.getAllHistory();
        ResponseEntity<DataResponse> postResponse = remotePostService.getAllPublishedPosts();

        ObjectMapper objectMapper = new ObjectMapper();
        List<HistoryResponse> historyResponseFin;
        List<Post> postResponseFin;
        try {
            String jsonData = objectMapper.writeValueAsString(historyResponse.getBody().getData());
            historyResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<HistoryResponse>>() {
            });
            jsonData = objectMapper.writeValueAsString(postResponse.getBody().getData());
            postResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<Post>>() {
            });
        } catch (JsonProcessingException e) {
            throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
        }

        AllPostInHistoryResponse response = AllPostInHistoryResponse.builder().historyWithPostDetailList(new LinkedList<>()).build();

        for (HistoryResponse value : historyResponseFin) {
            for (Post post : postResponseFin) {
                if (value.getPostId().equals(post.getPostId())) {
                    HistoryWithPostDetail historyWithPostDetail = HistoryWithPostDetail.builder()
                            .postId(value.getPostId())
                            .id(value.getHistory_id())
                            .userId(value.getUserId())
                            .viewDate(value.getViewDate())
                            .post(post)
                            .build();
                    response.getHistoryWithPostDetailList().addLast(historyWithPostDetail);
                }
            }
        }
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<AllPostInHistoryResponse> getAllPostInHistoryOnDate(String date) {
        ResponseEntity<DataResponse> historyResponse = remoteHistoryService.findHistoryByDate(date);
        ResponseEntity<DataResponse> postResponse = remotePostService.getAllPublishedPosts();

        ObjectMapper objectMapper = new ObjectMapper();
        List<History> historyResponseFin;
        List<Post> postResponseFin;
        try {
            String jsonData = objectMapper.writeValueAsString(historyResponse.getBody().getData());
            historyResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<History>>() {
            });
            jsonData = objectMapper.writeValueAsString(postResponse.getBody().getData());
            postResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<Post>>() {
            });
        } catch (JsonProcessingException e) {
            throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
        }

        AllPostInHistoryResponse response = AllPostInHistoryResponse.builder().historyWithPostDetailList(new LinkedList<>()).build();
        if(historyResponseFin != null) {
            for (History value : historyResponseFin) {
                for (Post post : postResponseFin) {
                    if (value.getPostId().equals(post.getPostId())) {
                        HistoryWithPostDetail historyWithPostDetail = HistoryWithPostDetail.builder()
                                .postId(value.getPostId())
                                .id(value.getId())
                                .userId(value.getUserId())
                                .viewDate(value.getViewDate())
                                .post(post)
                                .build();
                        response.getHistoryWithPostDetailList().addLast(historyWithPostDetail);
                    }
                }
            }
        }

        return ResponseEntity.ok(response);
    }

    public ResponseEntity<AllPostInHistoryResponse> getAllPostInHistoryContainWord(String word) {
        ResponseEntity<DataResponse> historyResponse = remoteHistoryService.getAllHistory();
        ResponseEntity<DataResponse> postResponse = remotePostService.getAllPublishedPosts();

        ObjectMapper objectMapper = new ObjectMapper();
        List<HistoryResponse> historyResponseFin;
        List<Post> postResponseFin;
        try {
            String jsonData = objectMapper.writeValueAsString(historyResponse.getBody().getData());
            historyResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<HistoryResponse>>() {
            });
            jsonData = objectMapper.writeValueAsString(postResponse.getBody().getData());
            postResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<Post>>() {
            });
        } catch (JsonProcessingException e) {
            throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
        }

        AllPostInHistoryResponse response = AllPostInHistoryResponse.builder().historyWithPostDetailList(new LinkedList<>()).build();

        for (HistoryResponse value : historyResponseFin) {
            for (Post post : postResponseFin) {
                if (value.getPostId().equals(post.getPostId()) && (post.getTitle().contains(word) || post.getContent().contains(word))) {
                    HistoryWithPostDetail historyWithPostDetail = HistoryWithPostDetail.builder()
                            .postId(value.getPostId())
                            .id(value.getHistory_id())
                            .userId(value.getUserId())
                            .viewDate(value.getViewDate())
                            .post(post)
                            .build();
                    response.getHistoryWithPostDetailList().addLast(historyWithPostDetail);
                }
            }
        }
        return ResponseEntity.ok(response);
    }

    public List<Post> getUserHomepagePostList() {
        ResponseEntity<DataResponse> response =remotePostService.getUserTop3ReplyPosts();
        if (response.getBody().getSuccess()) {
            DataResponse dataResponse = response.getBody();
            ObjectMapper objectMapper = new ObjectMapper();
            List<Post> posts = new ArrayList<>();
            System.out.println("posts: " + posts);
            if (dataResponse.getSuccess() && dataResponse.getData() != null) {
                try {
                    String jsonData = objectMapper.writeValueAsString(dataResponse.getData());
                    posts = objectMapper.readValue(jsonData, new TypeReference<List<Post>>() {
                    });
                } catch (JsonProcessingException e) {
                    throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
                }
            }
            return posts;
        } else {
            throw new PostServerException(response.getBody().getMessage());
        }
    }
    public User getUserById(int uid) {
        System.out.println(remoteUserService.getUserById(uid).getBody().getData());
        // return example {id=3, email=email3, firstName=normal, lastName=user, password=pass3, dateJoined=2014-07-25T15:18:10.000+00:00, type=2, profileImageURL=, validationToken=}
        LinkedHashMap<String, Object> li_hash_map = (LinkedHashMap<String, Object>) remoteUserService.getUserById(uid).getBody().getData();
        User user = new User();
//        for(Map.Entry<String, Object> entry :li_hash_map.entrySet()) System.out.println(entry.getKey());
        System.out.println(li_hash_map.get("email"));
        user.setId((Integer) li_hash_map.get("id"));
        user.setEmail((String) li_hash_map.get("email"));
        user.setFirstName((String) li_hash_map.get("firstName"));
//        user.setDateJoined((Date) li_hash_map.get("dateJoined"));
        user.setLastName((String) li_hash_map.get("lastName"));
        user.setPassword((String) li_hash_map.get("password"));
        user.setType((Integer) li_hash_map.get("type"));
        user.setProfileImageURL((String) li_hash_map.get("profileImageURL"));
        user.setValidationToken((String) li_hash_map.get("validationToken"));
        return user;
    }


    public ResponseEntity<DataResponse> createNewUser(InitialRegisterRequest request, MultipartFile image) {
        String imageUrl = "";
        System.out.println("request:" + image);
        if(image != null && !image.isEmpty()) {
            System.out.println(remoteFileService.uploadFileUrl(image));
            ResponseEntity<String> response = remoteFileService.uploadFileUrl(image);
            if(response != null && response.getBody() != null) {
                imageUrl = response.getBody();
            }
        }
        System.out.println("imageurl: " + imageUrl);
        return remoteUserService.register(RegisterRequest.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(request.getPassword())
                .profileImageURL(imageUrl).build());
    }

    public ResponseEntity<DataResponse> updateUserProfile(MultipartFile image, InitialRegisterRequest request, int id) {
        String imageUrl = "";
        System.out.println("request:" + image);
        if(image != null && !image.isEmpty()) {
            System.out.println(remoteFileService.uploadFileUrl(image));
            ResponseEntity<String> response = remoteFileService.uploadFileUrl(image);
            if(response != null && response.getBody() != null) {
                imageUrl = response.getBody();
            }
        }
        System.out.println("imageurl: " + imageUrl);
        System.out.println(request);
        return remoteUserService.modifiedUserProfile(UserPatchRequest.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(request.getPassword())
                .profileImageURL(imageUrl).build(), id);
    }

    public Object findAllPostsByStatusWithPageFilterByUsername(int postStatus, int page, int size, String username) {
        try{
            PostStatus status = PostStatus.valueOf(PostStatus.getLabelFromOrdinal(postStatus));
            ResponseEntity<DataResponse> postResponse = remotePostService.getAllPublishedPosts();

            ObjectMapper objectMapper = new ObjectMapper();
            List<Post> postResponseFin;
            try {
                String jsonData = objectMapper.writeValueAsString(postResponse.getBody().getData());
                postResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<Post>>() {
                });
            } catch (JsonProcessingException e) {
                throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
            }

            List<Post> posts = new ArrayList<>();
            Map<String, Object> response = new HashMap<>();
            List<Integer> Ids = new ArrayList<>();
            for(Post post: postResponseFin){
                Ids.add(post.getUserId());
            }
            ResponseEntity<DataResponse> userResponse = remoteUserService.getUsersByIdsGeneral(GeneralInfoRequest.builder().userIdList(Ids).build());
            List<UserGeneralDTO> userResponseFin;
            try {
                String jsonData = objectMapper.writeValueAsString(userResponse.getBody().getData());
                userResponseFin = objectMapper.readValue(jsonData, new TypeReference<List<UserGeneralDTO>>() {
                });
            } catch (JsonProcessingException e) {
                throw new ParsingModelException("JsonProcessingException occurred during parsing the response");
            }
            for(UserGeneralDTO user: userResponseFin){
                String name = user.getFirstName() + " " + user.getLastName();
                if(name.toUpperCase().contains(username.toUpperCase())){
                    System.out.println(name + " contains " + username);
                    for(Post post: postResponseFin){
                        if(post.getUserId() == user.getUserId()){
                            System.out.println("We have a post here whose owner is: " + name);
                            posts.add(post);
                        }
                    }
                }
            }

            List<Post> results = new ArrayList<>();
            for(int i = page*size; i<posts.size() && results.size() < size; i++){
                results.add(posts.get(i));
            }
            int totalPages = (int) Math.ceil(posts.size() / ((double) size));
            System.out.println("Total page: " + totalPages);
            response.put("posts", results);
            response.put("currentPage", page);
            response.put("totalItems", posts.size());
            response.put("totalPages", totalPages);
            return response;

        } catch (Exception e){
            return null;
        }
    }
}
