package com.example.compositeservice.service.remote;

import com.example.compositeservice.config.FeignSupportConfig;
import com.example.compositeservice.dto.common.DataResponse;
import com.example.compositeservice.dto.user.GeneralInfoRequest;
import com.example.compositeservice.dto.user.RegisterRequest;
import com.example.compositeservice.dto.user.UserPatchRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@FeignClient(name = "user-service", url="http://localhost:9000/user-service",
        configuration = FeignSupportConfig.class)
public interface RemoteUserService {
    @GetMapping("/user")
    ResponseEntity<DataResponse> getUserById(@RequestParam("userId") Integer userId);

    @PostMapping("/register")
    ResponseEntity<DataResponse> register(@RequestBody RegisterRequest request);

    @PatchMapping("/users/{id}")
    ResponseEntity<DataResponse> modifiedUserProfile(@RequestBody UserPatchRequest request, @PathVariable int id);

    @GetMapping("/user/{id}/general")
    ResponseEntity<DataResponse> getUserByIdGeneral(@PathVariable int id);

    @GetMapping("/users/general")
    ResponseEntity<DataResponse> getUsersByIdsGeneral(@RequestBody GeneralInfoRequest request);

}

