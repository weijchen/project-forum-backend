package com.example.compositeservice.service.remote;

import com.example.compositeservice.config.FeignSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(name = "file-service", url="http://localhost:9000/file-service",
        configuration = FeignSupportConfig.class)
public interface RemoteFileService {

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<String> uploadFile(@RequestPart(value = "file") MultipartFile file);

    @PostMapping(value = "/uploadUrl", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<String> uploadUrlFile(@RequestPart(value = "file") MultipartFile file);

    @GetMapping("/download/{fileName}")
    ResponseEntity<String> downloadFile(@PathVariable String fileName);

    @PostMapping(value = "/uploadUrl", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<String> uploadFileUrl(@RequestPart(value = "file") MultipartFile file);
}
