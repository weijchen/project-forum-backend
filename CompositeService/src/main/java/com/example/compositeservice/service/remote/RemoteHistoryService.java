package com.example.compositeservice.service.remote;

import com.example.compositeservice.dto.common.DataResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "history-service")
public interface RemoteHistoryService {

    @GetMapping("history-service/all")
    ResponseEntity<DataResponse> getAllHistory();

    @GetMapping("history-service/allPostId")
    ResponseEntity<DataResponse> getAllPostIdOfHistory();

    @GetMapping("history-service/historyByDate")
    ResponseEntity<DataResponse> findHistoryByDate(@RequestParam("date") String date);

    @GetMapping("history-service/allHistory")
    ResponseEntity<DataResponse> getAllHistoryPostIds();
}
