package com.example.compositeservice.service.remote;

import com.example.compositeservice.config.FeignSupportConfig;
import com.example.compositeservice.dto.common.DataResponse;
import com.example.compositeservice.dto.common.MessageResponse;
import com.example.compositeservice.dto.post.CreatePostRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "post-service", url="http://localhost:9000/post-service",
        configuration = FeignSupportConfig.class)
public interface RemotePostService {

    @GetMapping("/")
    ResponseEntity<DataResponse> getAllPublishedPosts();

    @GetMapping("{id}")
    ResponseEntity<DataResponse> getPost(@PathVariable String id);

    @GetMapping("/draft")
    ResponseEntity<DataResponse> getAllUnpublishedPosts();

    @GetMapping("/top3")
    ResponseEntity<DataResponse> getUserTop3ReplyPosts();

    @PostMapping(value = "/")
    ResponseEntity<MessageResponse> createPost(@RequestBody CreatePostRequest request);

    @GetMapping("/{id}/status")
    ResponseEntity<DataResponse> getAPostStatus(@PathVariable String id);
}
