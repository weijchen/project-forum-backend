package com.example.compositeservice.controller;

import com.example.compositeservice.dto.common.DataResponse;
import com.example.compositeservice.dto.common.MessageResponse;
import com.example.compositeservice.dto.common.UserHomePageInfoResponse;
import com.example.compositeservice.dto.post.InitialPostRequest;
import com.example.compositeservice.dto.user.InitialRegisterRequest;
import com.example.compositeservice.entity.Post;
import com.example.compositeservice.entity.PostStatus;
import com.example.compositeservice.entity.User;
import com.example.compositeservice.security.LoginUserAuthentication;
import com.example.compositeservice.service.CompositeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class CompositeController {
    private CompositeService compositeService;

    @Autowired
    public CompositeController(CompositeService compositeService) {
        this.compositeService = compositeService;
    }

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile(@RequestPart(value = "file") MultipartFile file) {
        System.out.println(file);
        return this.compositeService.uploadFile(file);
    }

    @GetMapping("/posts")
    public DataResponse getAllPublishedPosts() throws JsonProcessingException {
        return DataResponse.builder().success(true)
                .data(this.compositeService.getAllPublishedPosts())
                .message("Successfully get all the published posts").build();
    }
    @GetMapping("with-jpa/filterByName")
    public ResponseEntity<DataResponse> getAllPublishedPostsFilterByName(
            @RequestParam(required = false) Integer postStatus,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "6") int size,
            @RequestParam(required = false, defaultValue = "") String username) {
        int status;
        if (postStatus == null) {
            status = PostStatus.Published.ordinal();
        } else {
            status = postStatus;
        }
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(compositeService.findAllPostsByStatusWithPageFilterByUsername(status, page, size, username))
                .message("Successfully get all published posts")
                .build(), HttpStatus.OK);
    }
    @PostMapping("/posts")
    public MessageResponse createPost(@ModelAttribute InitialPostRequest request,
                                      @RequestPart(value = "images", required = false) MultipartFile[] images,
                                      @RequestPart(value = "files", required = false) MultipartFile[] files) {

        return compositeService.createNewPost(request, images, files);
    }

    @GetMapping("/posts/{id}")
    public DataResponse getPostById(@PathVariable String id) {
        return DataResponse.builder().success(true)
                .data(this.compositeService.getPostById(id))
                .message("Successfully get all the published posts").build();
    }

    @GetMapping("/posts/draft")
    public DataResponse getAllUnpublishedPosts() {
        return DataResponse.builder().success(true)
                .data(this.compositeService.getAllUnpublishedPosts())
                .message("Successfully get all draft posts").build();
    }

    @GetMapping("/posts/top3")
    public DataResponse getUserTop3ReplyPosts() {
        return DataResponse.builder().success(true)
                .data(this.compositeService.getUserTop3ReplyPosts())
                .message("Successfully get user top 3 most reviews posts").build();
    }

    @GetMapping("/history")
    public DataResponse getAllPostInHistory() {
        return DataResponse.builder().success(true)
                .data(this.compositeService.getAllPostInHistory())
                .message("Successfully get viewed post in history").build();
    }

    @GetMapping("/historyByDate")
    public DataResponse getAllPostInHistoryOnDate(@RequestParam("date") String date) {
        return DataResponse.builder().success(true)
                .data(this.compositeService.getAllPostInHistoryOnDate(date))
                .message("Successfully get viewed post in history on date").build();
    }

    @GetMapping("/historyContainWord")
    public DataResponse getAllPostInHistoryContainWord(@RequestParam("word") String word) {
        return DataResponse.builder().success(true)
                .data(this.compositeService.getAllPostInHistoryContainWord(word))
                .message("Successfully get viewed post in history contain word").build();
    }

    @GetMapping("/users/home")
    public ResponseEntity<UserHomePageInfoResponse> getUserHomePageInfo() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        User user = compositeService.getUserById(userId);
        List<Post> postList = compositeService.getUserHomepagePostList();
        UserHomePageInfoResponse res = UserHomePageInfoResponse.builder().user(user).postList(postList).build();
        return ResponseEntity.ok(res);
    }

    @PostMapping("/register")
    public ResponseEntity<DataResponse> registerUser(@ModelAttribute InitialRegisterRequest request,
                                                     @RequestPart(value = "image", required = false) MultipartFile image) {
        return compositeService.createNewUser(request, image);
    }

    @PatchMapping(value = "/users/{id}", consumes = {"multipart/form-data"})
    public ResponseEntity<DataResponse> updateUserProfile(@RequestParam String firstName,
                                                          @RequestParam String lastName,
                                                          @RequestParam String email,
                                                          @RequestParam String password,
                                                          @RequestParam(value = "image", required = false) MultipartFile image, @PathVariable int id) {
        InitialRegisterRequest request = InitialRegisterRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .password(password)
                .build();
        return compositeService.updateUserProfile(image, request, id);
    }

    @GetMapping("/cache/{id}")
    public ResponseEntity<MessageResponse> updateCache(@PathVariable String id) {
        compositeService.updateCache(id);
        return new ResponseEntity<>(MessageResponse.builder().success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully update cache when needed")
                .build(), HttpStatus.OK);
    }
}
