package com.example.compositeservice.entity;

import lombok.*;
import java.util.Date;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private Date dateJoined;
    private int type;
    private String profileImageURL;
    private String validationToken;


}