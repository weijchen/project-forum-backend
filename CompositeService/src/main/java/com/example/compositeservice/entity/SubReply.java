package com.example.compositeservice.entity;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SubReply {
    private int userId;
    private String comment;
    private boolean isActive;
    private Date dateCreated;
}
