package com.example.compositeservice.entity;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HistoryWithPostDetail {
    private Integer id;
    private Integer userId;
    private String postId;
    private Timestamp viewDate;
    private Post post;
}
