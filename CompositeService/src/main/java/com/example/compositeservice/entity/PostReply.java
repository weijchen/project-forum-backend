package com.example.compositeservice.entity;

import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PostReply {
    private int userId;
    private String comment;
    private boolean isActive;
    private Date dateCreated;
    private List<SubReply> subReplies;
}

