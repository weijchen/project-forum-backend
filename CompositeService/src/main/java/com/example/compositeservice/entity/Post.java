package com.example.compositeservice.entity;

import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private String postId;
    private int userId;
    private String title;
    private String content;
    private Boolean isArchived;
    private PostStatus status;
    private Date dateCreated;
    private Date dateModified;
    private List<String> images;
    private List<String> attachments;
    private List<PostReply> postReplies;
}
