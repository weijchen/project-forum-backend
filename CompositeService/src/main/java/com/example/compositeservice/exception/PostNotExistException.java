package com.example.compositeservice.exception;

public class PostNotExistException extends RuntimeException {
    private String message;
    public PostNotExistException(String message) {
        super(message);
        this.message = message;
    }
}