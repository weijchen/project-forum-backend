package com.example.compositeservice.exception;

public class PostServerException extends RuntimeException {
    private String message;
    public PostServerException(String message) {
        super(message);
        this.message = message;
    }
}
