package com.example.compositeservice.exception;

public class EntryNotCorrectException extends RuntimeException {
    private String message;
    public EntryNotCorrectException(String message) {
        super(message);
        this.message = message;
    }
}
