package com.example.compositeservice.exception;

public class ParsingModelException extends RuntimeException{
    private String message;
    public ParsingModelException(String message) {
        super(message);
        this.message = message;
    }
}
