package com.example.compositeservice.security;

import feign.RequestInterceptor;
import feign.RequestTemplate;
public class JwtTokenInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        String token = JwtContext.getJwtToken();
        template.header("Authorization", token);
    }
}
