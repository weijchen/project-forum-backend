package com.example.compositeservice.dto.user;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GeneralInfoRequest {
    private List<Integer> userIdList;
}
