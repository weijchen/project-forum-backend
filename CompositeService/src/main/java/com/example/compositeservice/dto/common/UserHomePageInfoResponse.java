package com.example.compositeservice.dto.common;

import com.example.compositeservice.entity.Post;
import com.example.compositeservice.entity.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@Builder
@ToString
public class UserHomePageInfoResponse {
    User user;
    List<Post> postList;
}
