package com.example.compositeservice.dto.post;

import com.example.compositeservice.entity.HistoryWithPostDetail;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;

@Builder
@Setter
@Getter
public class AllPostInHistoryResponse {
    LinkedList<HistoryWithPostDetail> historyWithPostDetailList;
}
