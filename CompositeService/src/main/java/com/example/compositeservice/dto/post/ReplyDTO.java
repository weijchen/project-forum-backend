package com.example.compositeservice.dto.post;

import lombok.*;

import java.util.Date;
import java.util.List;
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReplyDTO {
    private int userId;
    private String firstName;
    private String lastName;
    private String profileImageURL;
    private String comment;
    private boolean isActive;
    private Date dateCreated;
    private List<SubreplyDTO> subReplies;
}
