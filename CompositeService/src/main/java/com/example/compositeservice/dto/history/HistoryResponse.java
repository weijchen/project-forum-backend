package com.example.compositeservice.dto.history;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HistoryResponse {
    private Integer history_id;
    private Integer userId;
    private String postId;
    private Timestamp viewDate;
}