package com.example.compositeservice.dto.user;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class InitialRegisterRequest {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
