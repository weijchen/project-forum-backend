package com.example.compositeservice.dto.post;

import com.example.compositeservice.entity.PostStatus;
import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InitialPostRequest {
    private String title;
    private String content;
    private PostStatus status;
}
