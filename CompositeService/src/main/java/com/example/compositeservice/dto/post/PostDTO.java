package com.example.compositeservice.dto.post;

import com.example.compositeservice.entity.PostStatus;
import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {
    private String postId;
    private int userId;
    private String firstName;
    private String lastName;
    private String profileImageURL;
    private String title;
    private String content;
    private Boolean isArchived;
    private PostStatus status;
    private Date dateCreated;
    private Date dateModified;
    private List<String> images;
    private List<String> attachments;
    private List<ReplyDTO> postReplies;
}
