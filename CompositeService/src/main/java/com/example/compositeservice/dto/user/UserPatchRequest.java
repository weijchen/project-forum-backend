package com.example.compositeservice.dto.user;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserPatchRequest {
    String email;
    String firstName;
    String lastName;
    String password;
    String profileImageURL;
}