package com.example.compositeservice.dto.common;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
@Getter
public class DataResponse {
    private Boolean success;
    private String message;
    private Object data;
}