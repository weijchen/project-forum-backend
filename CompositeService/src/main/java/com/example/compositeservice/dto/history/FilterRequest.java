package com.example.compositeservice.dto.history;

import lombok.*;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilterRequest {
    private String date;
}
