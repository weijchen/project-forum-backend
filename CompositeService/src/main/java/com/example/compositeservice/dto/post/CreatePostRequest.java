package com.example.compositeservice.dto.post;

import com.example.compositeservice.entity.PostStatus;
import lombok.*;
import java.util.List;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatePostRequest {
    private String title;
    private String content;
    private PostStatus status;
    private List<String> images;
    private List<String> attachments;
}
