package com.example.compositeservice.dto.post;

import com.example.compositeservice.entity.Post;
import lombok.*;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SinglePostResponse {
    private Post post;
    private String firstName;
    private String lastName;
    private String profileImageURL;
}
