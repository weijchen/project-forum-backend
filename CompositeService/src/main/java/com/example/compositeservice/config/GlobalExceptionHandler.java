package com.example.compositeservice.config;

import com.example.compositeservice.dto.common.MessageResponse;
import com.example.compositeservice.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = {StatusNotCorrectException.class})
    public ResponseEntity<MessageResponse> statusNotCorrectException(StatusNotCorrectException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {ParsingModelException.class})
    public ResponseEntity<MessageResponse> jsonProcessingException(ParsingModelException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {PostServerException.class})
    public ResponseEntity<MessageResponse> postServerException(PostServerException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {UserNotExistException.class})
    public ResponseEntity<MessageResponse> userNotExistException(UserNotExistException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {PostNotExistException.class})
    public ResponseEntity<MessageResponse> postNotExistException(PostNotExistException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {EntryNotCorrectException.class})
    public ResponseEntity<MessageResponse> entryNotCorrectException(EntryNotCorrectException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }
}
