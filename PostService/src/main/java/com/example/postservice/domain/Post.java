package com.example.postservice.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Post {
    @Id
    private String postId;

    @ApiModelProperty(value = "The userId of the user that create the post")
    private int userId;

    @ApiModelProperty(value = "The title the post")
    private String title;

    @ApiModelProperty(value = "The content of the post")
    private String content;

    private Boolean isArchived;

    @ApiModelProperty(value = "The status the post",
    allowableValues = "Unpublished, Published, Hidden, Banned, Deleted")
    private PostStatus status;

    private Date dateCreated;

    private Date dateModified;

    private List<String> images;

    private List<String> attachments;

//    @DBRef
    private List<PostReply> postReplies;


}
