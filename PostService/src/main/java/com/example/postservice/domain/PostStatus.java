package com.example.postservice.domain;

public enum PostStatus {
    Unpublished("Unpublished"),
    Published("Published"),
    Hidden("Hidden"),
    Banned("Banned"),
    Deleted("Deleted");

    public final String label;

    PostStatus(String label) {
        this.label = label;
    }

    public static String getLabelFromOrdinal(int ordinal) {
        for (PostStatus e : PostStatus.values()) {
            if (e.ordinal() == ordinal) return e.label;
        }
        return null;
    }
}
