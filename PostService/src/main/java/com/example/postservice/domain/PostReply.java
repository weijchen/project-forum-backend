package com.example.postservice.domain;

import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
public class PostReply {
    private long userId;
    private String comment;
    private boolean isActive;
    private Date dateCreated;
    private List<SubReply> subReplies;
}

