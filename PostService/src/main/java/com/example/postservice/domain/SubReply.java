package com.example.postservice.domain;

import lombok.*;
import java.util.Date;

@Getter
@Setter
@Builder
@ToString
public class SubReply {
    private long userId;
    private String comment;
    private boolean isActive;
    private Date dateCreated;
}
