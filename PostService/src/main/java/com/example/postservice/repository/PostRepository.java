package com.example.postservice.repository;

import com.example.postservice.domain.Post;
import com.example.postservice.domain.PostStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends MongoRepository<Post, String> {
    List<Post> findByStatusOrderByDateCreatedDesc(PostStatus status);
    List<Post> findByStatusAndUserIdOrderByDateCreatedDesc(PostStatus status, int userId);
    List<Post> findPostByUserId(int userId);
    List<Post> findPostByStatusNotIn(List<PostStatus> statusList);
    Post findPostByPostId(String postId);
    Page<Post> findPostsByStatusIsOrderByDateCreatedDesc(PostStatus status, Pageable pageable);
    Page<Post> findPostsByStatusIsOrderByDateCreatedAsc(PostStatus status, Pageable pageable);
//    Page<Post> findPostsByStatusIsOrderByDateCreatedAsc(PostStatus status, Pageable pageable);
//    Page<Post> findPostsByStatusIsOrderByPostRepliesDesc(PostStatus status, Pageable pageable);
}
