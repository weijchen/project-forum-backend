package com.example.postservice.controller;

import com.example.postservice.dto.common.MessageResponse;
import com.example.postservice.dto.reply.CreateReplyRequest;
import com.example.postservice.security.LoginUserAuthentication;
import com.example.postservice.service.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/posts")
public class ReplyController {
    private final ReplyService replyService;

    @Autowired
    public ReplyController(ReplyService replyService) {
        this.replyService = replyService;
    }


    @PostMapping("/{postId}/replies")
    public ResponseEntity<MessageResponse> createReply(@PathVariable String postId, @RequestBody CreateReplyRequest createReplyRequest) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        replyService.createReply(userId, postId, createReplyRequest);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully create a reply to post " + postId)
                .build(), HttpStatus.OK);
    }


    @DeleteMapping("/{postId}/replies/{replyId}")
    public ResponseEntity<MessageResponse> deleteReply(@PathVariable String postId, @PathVariable int replyId) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        boolean isAdmin = auth.getAuthorities().toString().contains("admin_read");
        replyService.deleteReply(userId, postId, replyId - 1, isAdmin);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully delete a reply of a post " + postId)
                .build(), HttpStatus.OK);
    }

    @PostMapping("{postId}/replies/{replyId}/sub-replies")
    public ResponseEntity<MessageResponse> createSubReply(@PathVariable String postId, @PathVariable int replyId, @RequestBody CreateReplyRequest createReplyRequest) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        replyService.createSubReply(userId, postId, replyId - 1, createReplyRequest);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully create a sub-reply to reply " + replyId + " for post" + postId)
                .build(), HttpStatus.OK);
    }

    @DeleteMapping("/{postId}/replies/{replyId}/sub-replies/{subreplyId}")
    public ResponseEntity<MessageResponse> deleteSubReply(@PathVariable String postId, @PathVariable int replyId, @PathVariable int subreplyId) {
        System.out.println("controller");
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        boolean isAdmin = auth.getAuthorities().toString().contains("admin_read");
        System.out.println("postId: " + postId);
        System.out.println("replyId: " + replyId);
        System.out.println("subreplyId: " + subreplyId);
        replyService.deleteSubReply(userId, postId, replyId - 1, subreplyId - 1, isAdmin);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully delete a subreply of a post " + postId)
                .build(), HttpStatus.OK);
    }

// TODO: Not sure if this section will be used in the future, Keep now

//    @GetMapping("/{postId}/replies")
//    public AllReplyResponse getAllPostReplies(@PathVariable String postId) {
//        return AllReplyResponse.builder()
//                .serviceStatus(ServiceStatus.builder()
//                        .success(true).build())
//                .replyList(replyService.getAllPostReplies(postId))
//                .build();
//    }


//    @GetMapping("{postId}/replies/{replyId}/sub-replies")
//    public AllSubReplyResponse getAllSubReplies(@PathVariable String postId, @PathVariable String replyId) {
//        return AllSubReplyResponse.builder()
//                .serviceStatus(ServiceStatus.builder()
//                        .success(true).build())
//                .subReplyList(replyService.getAllSubReplies(postId, replyId))
//                .build();
//    }
}
