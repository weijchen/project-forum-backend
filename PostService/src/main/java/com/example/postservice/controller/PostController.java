package com.example.postservice.controller;

import com.example.postservice.domain.PostStatus;
import com.example.postservice.dto.common.DataResponse;
import com.example.postservice.dto.common.MessageResponse;
import com.example.postservice.dto.post.CreatePostRequest;
import com.example.postservice.dto.post.PostResponse;
import com.example.postservice.dto.post.UpdatePostRequest;
import com.example.postservice.security.LoginUserAuthentication;
import com.example.postservice.service.PostService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PostController {
    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("")
    @ApiOperation(value = "Get all published posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllPublishedPosts() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.findAllPublishedPosts())
                .message("Successfully get all published posts")
                .build(), HttpStatus.OK);
    }

    @GetMapping("with-jpa")
    @ApiOperation(value = "Get all published posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllPublishedPosts(
            @RequestParam(required = false) Integer postStatus,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "6") int size) {
        int status;
        if (postStatus == null) {
            status = PostStatus.Published.ordinal();
        } else {
            status = postStatus;
        }
        System.out.println("GETTING ALL PUBLISHED POST WITH PAGES");
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.findAllPostsByStatusWithPage(status, page, size))
                .message("Successfully get all posts")
                .build(), HttpStatus.OK);
    }

    @GetMapping("/banned")
    @ApiOperation(value = "Get all banned posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllBannedPosts() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.getBannedPosts())
                .message("Successfully get all banned posts.")
                .build(), HttpStatus.OK);
    }

    @GetMapping("with-jpa/sortByDates")
    @ApiOperation(value = "Get all published posts filter By Dates", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllPublishedPostsSortByDates(
            @RequestParam(required = false) Integer postStatus,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "6") int size,
            @RequestParam(required = false, defaultValue = "DESC") String filterStatus) {
        int status;
        if (postStatus == null) {
            status = PostStatus.Published.ordinal();
        } else {
            status = postStatus;
        }
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.findAllPostsByStatusWithPageSortByDates(status, page, size, filterStatus))
                .message("Successfully get all published posts")
                .build(), HttpStatus.OK);
    }
    @GetMapping("with-jpa/sortByReplies")
    @ApiOperation(value = "Get all published posts filter By Replies", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllPublishedPostsSortByReplies(
            @RequestParam(required = false) Integer postStatus,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "6") int size,
            @RequestParam(required = false, defaultValue = "DESC") String filterStatus) {
        int status;
        if (postStatus == null) {
            status = PostStatus.Published.ordinal();
        } else {
            status = postStatus;
        }
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.findAllPostsByStatusWithPageSortByReplies(status, page, size, filterStatus))
                .message("Successfully get all published posts")
                .build(), HttpStatus.OK);
    }
//
//    @GetMapping("/numberOfReplies")
//    @ApiOperation(value = "Get all published posts", response = DataResponse.class)
//    public ResponseEntity<DataResponse> getAllPublishedPostOrderByReplies(
//            @RequestParam(defaultValue = "0") int page,
//            @RequestParam(defaultValue = "6") int size) {
//
//        return new ResponseEntity<>(DataResponse.builder()
//                .success(true)
//                .data(postService.findPublishedPostsByNumberOfReplies(page, size))
//                .message("Successfully get all published posts")
//                .build(), HttpStatus.OK);
//    }

    @GetMapping("/myPosts")
    @ApiOperation(value = "Get user published posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllPublishedPostsByUid() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.getUserPosts())
                .message("Successfully get all published posts for user.")
                .build(), HttpStatus.OK);
    }

    @GetMapping("/myPosts/banned")
    @ApiOperation(value = "Get user banned posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllBannedPostsByUid() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.getUserBannedPosts())
                .message("Successfully get all banned posts for user.")
                .build(), HttpStatus.OK);
    }

    @GetMapping("/myPosts/hidden")
    @ApiOperation(value = "Get user hidden posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllHiddenPostsByUid() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.getUserHiddenPosts())
                .message("Successfully get all hidden posts for user.")
                .build(), HttpStatus.OK);
    }

    @GetMapping("/myPosts/deleted")
    @ApiOperation(value = "Get user deleted posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getAllDeletedPostsByUid() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.getUserDeletedPosts())
                .message("Successfully get all deleted posts for user.")
                .build(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('recover')")
    @GetMapping("/all")
    @ApiOperation(value = "Get all posts except hidden and unpublished", response = DataResponse.class,
            notes = "Admin only")
    public ResponseEntity<DataResponse> getAllPostsAdmin() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.getAllPostsAdmin())
                .message("Successfully get all posts that are not hidden and unpublished.")
                .build(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('recover')")
    @GetMapping("/all/deleted")
    @ApiOperation(value = "Get all deleted posts", response = DataResponse.class,
            notes = "Admin only")
    public ResponseEntity<DataResponse> getAllDeletedPostsAdmin() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .data(postService.getAllDeletedPostsAdmin())
                .message("Successfully get deleted posts")
                .build(), HttpStatus.OK);
    }

    // the post can be a draft or published. It bases on the status in the request
    @PostMapping("")
    @PreAuthorize("hasAuthority('write')")
    @ApiOperation(value = "Create a new post",
            notes = "The created post can be an unpublished or published based on the status value in the request",
            response = MessageResponse.class)
    public ResponseEntity<MessageResponse> createPost(@RequestBody CreatePostRequest request) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        postService.createNewPost(request, userId);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .message("Successfully create the post")
                .statusCode(HttpStatus.OK.value())
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('write')")
    @ApiOperation(value = "Update a post", response = MessageResponse.class)
    public ResponseEntity<MessageResponse> updatePost(@RequestBody UpdatePostRequest request, @PathVariable String id) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        postService.updatePost(request, id, userId);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully update the post")
                .build(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    @ApiOperation(value = "Get a post", response = DataResponse.class)
    public ResponseEntity<DataResponse> getPost(@PathVariable String id) {
        return new ResponseEntity<>(DataResponse.builder().data(postService.getPost(id))
                .success(true).message("Successfully get the post").build(), HttpStatus.OK);
    }

    @GetMapping("/draft")
    @ApiOperation(value = "Get all unpublished posts", response = PostResponse.class)
    public ResponseEntity<DataResponse> getAllUnpublishedPosts() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .message("Get all unpublished posts")
                .data(postService.findAllUnpublishedPosts(userId))
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}/hidden")
    @PreAuthorize("hasAuthority('write')")
    @ApiOperation(value = "Set a post as Hidden", response = MessageResponse.class,
            notes = "Only post owner allow to hide the post")
    public ResponseEntity<MessageResponse> hidePost(@PathVariable String id) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        postService.updatePostStatus(id, userId);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully update the post")
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}/archive")
    @PreAuthorize("hasAuthority('write')")
    @ApiOperation(value = "Change the setting on whether allow others to reply on the post", response = MessageResponse.class,
            notes = "Post owner only")
    public ResponseEntity<MessageResponse> allowReplyPost(@PathVariable String id) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        postService.updateAllowReplyPost(id, userId);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully update the post")
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}/publish")
    @PreAuthorize("hasAuthority('write')")
//    @Secured({"write", "delete", "ban_unban", "recover"})
    @ApiOperation(value = "Set a post to publish", response = MessageResponse.class,
            notes = "Post owner can change post that is hidden or unpublished. Ban or delete post need admin")
    public ResponseEntity<MessageResponse> publishPost(@PathVariable String id) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        List<GrantedAuthority> authorities = (List<GrantedAuthority>) auth.getAuthorities();
        postService.setPostPublish(id, userId, authorities);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully publish the post")
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}/banned")
    @PreAuthorize("hasAuthority('ban_unban')")
    @ApiOperation(value = "Ban or unban a post", response = MessageResponse.class,
            notes = "Admin only")
    public ResponseEntity<MessageResponse> banPost(@PathVariable String id) {
        postService.banPost(id);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully change the status of the post")
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}/delete")
    @PreAuthorize("hasAuthority('delete')")
    @ApiOperation(value = "Delete a post", response = MessageResponse.class,
            notes = "Owner only")
    public ResponseEntity<MessageResponse> deletePost(@PathVariable String id) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        //List<GrantedAuthority> authorities = (List<GrantedAuthority>)auth.getAuthorities();
        postService.deletePost(id, userId);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully delete the post")
                .build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}/recover")
    @PreAuthorize("hasAuthority('recover')")
    @ApiOperation(value = "Recover a post", response = MessageResponse.class,
            notes = "Owner only")
    public ResponseEntity<MessageResponse> recoverPost(@PathVariable String id) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        //List<GrantedAuthority> authorities = (List<GrantedAuthority>)auth.getAuthorities();
        postService.recoverPost(id);
        return new ResponseEntity<>(MessageResponse.builder()
                .success(true)
                .statusCode(HttpStatus.OK.value())
                .message("Successfully recover the post")
                .build(), HttpStatus.OK);
    }

    @GetMapping("/history")
    @ApiOperation(value = "Get history view of posts", response = DataResponse.class)
    public ResponseEntity<DataResponse> getHistoryViewPosts() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .message("Get all the user history view posts")
                .data(postService.getHistoryViewPosts(userId))
                .build(), HttpStatus.OK);
    }

    @GetMapping("/top3")
    @ApiOperation(value = "Get Top 3 most reviews post of the user", response = DataResponse.class)
    public ResponseEntity<DataResponse> getUserTop3ReplyPosts() {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .message("Get top 3 most replies posts")
                .data(postService.getUserTop3ReplyPosts())
                .build(), HttpStatus.OK);
    }

    @GetMapping("/{id}/status")
    public ResponseEntity<DataResponse> getAPostStatus(@PathVariable String id) {
        return new ResponseEntity<>(DataResponse.builder()
                .success(true)
                .message("Get the post status")
                .data(postService.getAPostStatus(id))
                .build(), HttpStatus.OK);
    }
}
