package com.example.postservice.service;

import com.example.postservice.domain.Post;
import com.example.postservice.domain.PostReply;
import com.example.postservice.domain.PostStatus;
import com.example.postservice.domain.SubReply;
import com.example.postservice.dto.reply.CreateReplyRequest;
import com.example.postservice.exception.EditNotAllowedException;
import com.example.postservice.exception.PostNotExistException;
import com.example.postservice.exception.ReplyNotAllowedException;
import com.example.postservice.exception.ReplyNotExistException;
import com.example.postservice.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

@Service
public class ReplyService {
    private final PostRepository postRepository;

    @Autowired
    public ReplyService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }


    public void createReply(int userId, String postId, CreateReplyRequest createReplyRequest) {
        System.out.println(postId);
        Optional<Post> postOptional = postRepository.findById(postId);
        if (postOptional.isPresent()) {
            Post post = postOptional.get();

            if (post.getIsArchived() || post.getStatus() != PostStatus.Published) {
                throw new ReplyNotAllowedException("Post is either unpublished or in archive mode, cannot reply");
            }
            // Create a new reply
            PostReply postReply = PostReply.builder()
                    .userId(userId)
                    .comment(createReplyRequest.getComment())
                    .subReplies(new ArrayList<>())
                    .isActive(true)
                    .dateCreated(new Date())
                    .build();

            if (post.getPostReplies().size() == 0) {
                post.setPostReplies(new ArrayList<>());
            }

            // Add the reply to the post's reply list
            post.getPostReplies().add(postReply);
            // Save the post
            postRepository.save(post);
        } else {
            throw new PostNotExistException("Post not exist");
        }
    }

    public void createSubReply(int userId, String postId, int replyIndex, CreateReplyRequest createReplyRequest) {
        Optional<Post> postOptional = postRepository.findById(postId);

        if (!postOptional.isPresent()) {
            throw new PostNotExistException("No such post with id " + postId);
        }

        Post post = postOptional.get();

        if (post.getIsArchived()) {
            throw new ReplyNotAllowedException("Post is in archive mode, cannot reply");
        }

        if (replyIndex > post.getPostReplies().size()) {
            throw new ReplyNotExistException("No such reply with id " + replyIndex + " for post with id " + postId);
        }

        PostReply reply = post.getPostReplies().get(replyIndex);

        SubReply subReply = SubReply.builder()
                .userId(userId)
                .comment(createReplyRequest.getComment())
                .isActive(true)
                .dateCreated(new Date())
                .build();

        reply.getSubReplies().add(subReply);
        postRepository.save(post);
    }

    public void deleteReply(int userId, String postId, int replyIndex, boolean isAdmin) {
        Optional<Post> postOptional = postRepository.findById(postId);

        if (!postOptional.isPresent()) {
            throw new PostNotExistException("No such post with id " + postId);
        }

        Post post = postOptional.get();

        if (replyIndex > post.getPostReplies().size()) {
            throw new ReplyNotExistException("No such reply with id " + replyIndex + " for post with id " + postId);
        }

        PostReply reply = post.getPostReplies().get(replyIndex);

        // only creator, post owner and admin can delete the reply
        if (userId == post.getUserId() || userId == reply.getUserId() || isAdmin) {
            reply.setActive(false);
            postRepository.save(post);
        } else {
            throw new EditNotAllowedException("Only admin, creator, and the owner of the reply can delete this reply");
        }
    }

    public void deleteSubReply(int userId, String postId, int replyIndex, int subReplyIndex, boolean isAdmin) {
        Optional<Post> postOptional = postRepository.findById(postId);

        if (!postOptional.isPresent()) {
            throw new PostNotExistException("No such post with id " + postId);
        }

        Post post = postOptional.get();

        if (replyIndex > post.getPostReplies().size()) {
            throw new ReplyNotExistException("No such reply with id " + replyIndex + " for post with id " + postId);
        }

        PostReply reply = post.getPostReplies().get(replyIndex);

        if (subReplyIndex > reply.getSubReplies().size()) {
            throw new ReplyNotExistException("No such subreply with id " + subReplyIndex + " for reply with id " + replyIndex);
        }

        SubReply subReply = post.getPostReplies().get(replyIndex).getSubReplies().get(subReplyIndex);

        // only creator, post owner and admin can delete the subreply
        if (userId == post.getUserId() || userId == subReply.getUserId() || isAdmin) {
            subReply.setActive(false);
            postRepository.save(post);
        } else {
            throw new EditNotAllowedException("Only admin, creator, and the owner of the subreply can delete this subreply");

        }
    }


// TODO: Not sure if this section will be used in the future, Keep now

//    public List<PostReply> getAllPostReplies(String postId) {
//        Post post = postRepository.findPostByPostId(postId);
//        List<PostReply> replyList = post.getPostReplies();
//        return replyList;
//    }

//    public List<SubReply> getAllSubReplies(String postId, String replyId) {
//        Post post = postRepository.findPostByPostId(postId);
//        if (post == null) {
//            throw new PostNotExistException("No such post with id " + postId);
//        }
//
//        Optional<PostReply> replyOptional = post.getPostReplies().stream()
//                .filter(reply -> reply.getId() == replyId)
//                .findFirst();
//
//        if (replyOptional.isPresent()) {
//            throw new ReplyNotExistException("No such reply with id " + replyId + " for post with id " + postId);
//        }
//
//        PostReply reply = replyOptional.get();
//        List<SubReply> subReplies = reply.getSubReplies();
//
//        return subReplies;
//    }
}
