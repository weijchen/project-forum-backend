package com.example.postservice.service.remote;

import com.example.postservice.dto.common.DataResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "post-history-service", url="http://localhost:9000/post-history-service")
public interface RemotePostHistoryService {

    @GetMapping("/history")
    DataResponse getUserAllHistory();
}