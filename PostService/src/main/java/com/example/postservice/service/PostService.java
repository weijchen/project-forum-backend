package com.example.postservice.service;

import com.example.postservice.domain.Post;
import com.example.postservice.domain.PostReply;
import com.example.postservice.domain.PostStatus;
import com.example.postservice.dto.history.HistoryResponse;
import com.example.postservice.dto.post.CreatePostRequest;
import com.example.postservice.dto.post.UpdatePostRequest;
import com.example.postservice.exception.AccessNotAllowedException;
import com.example.postservice.exception.EditNotAllowedException;
import com.example.postservice.exception.PostNotExistException;
import com.example.postservice.exception.StatusNotCorrectException;
import com.example.postservice.repository.PostRepository;
import com.example.postservice.security.LoginUserAuthentication;
import com.example.postservice.service.remote.RemoteFileService;
import com.example.postservice.service.remote.RemotePostHistoryService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostService {
    private final PostRepository postRepository;
    private final RemoteFileService remoteFileService;
    private final RemotePostHistoryService remotePostHistoryService;

    @Autowired
    public PostService(PostRepository postRepository, RemoteFileService remoteFileService,
                       RemotePostHistoryService remotePostHistoryService) {
        this.postRepository = postRepository;
        this.remoteFileService = remoteFileService;
        this.remotePostHistoryService = remotePostHistoryService;
    }

    public List<Post> findAllPublishedPosts() {
        return postRepository.findByStatusOrderByDateCreatedDesc(PostStatus.Published);
    }

    public Map<String, Object> findAllPostsByStatusWithPage(int postStatus, int page, int size) {
        try {
            List<Post> posts = new ArrayList<Post>();
            Pageable paging = PageRequest.of(page, size);

            PostStatus status = PostStatus.valueOf(PostStatus.getLabelFromOrdinal(postStatus));

            Page<Post> pageTuts = postRepository.findPostsByStatusIsOrderByDateCreatedDesc(status, paging);

            posts = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("posts", posts);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public Object findAllPostsByStatusWithPageSortByDates(int postStatus, int page, int size, String filterStatus) {
        try{
            List<Post> posts = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size);
            PostStatus status = PostStatus.valueOf(PostStatus.getLabelFromOrdinal(postStatus));
            Page<Post> pageTuts;
            if(filterStatus.equals("DESC")) {
                System.out.println("Sort By dates descending");
                pageTuts = postRepository.findPostsByStatusIsOrderByDateCreatedDesc(status, paging);
            } else {
                System.out.println("Sort By dates ascending");
                pageTuts = postRepository.findPostsByStatusIsOrderByDateCreatedAsc(status, paging);
            }
            posts = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("posts", posts);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());
            return response;
        } catch (Exception e) {
            return null;
        }
    }
    public Object findAllPostsByStatusWithPageSortByReplies(int postStatus, int page, int size, String filterStatus) {
        try{
            PostStatus status = PostStatus.valueOf(PostStatus.getLabelFromOrdinal(postStatus));
            List<Post> allPublished = postRepository.findByStatusOrderByDateCreatedDesc(status);
            List<Post> posts = new ArrayList<>();
            Map<String, Object> response = new HashMap<>();
            Comparator<Post> repliesComparator = Comparator.comparingInt(post -> post.getPostReplies().size());
            if(filterStatus.equals("DESC")) {
                System.out.println("Sort By Replies descending");
                repliesComparator = repliesComparator.reversed();
            } else {
                System.out.println("Sort By Replies ascending");
            }
            Collections.sort(allPublished, repliesComparator);
            for(Post post: allPublished){
                System.out.println("Post: " + post.getPostId() + " size: " + post.getPostReplies().size());
            }

            for(int i = page*size; i<=allPublished.size() && posts.size() < size; i++){
                posts.add(allPublished.get(i));
            }

            int totalPages = (int) Math.ceil(allPublished.size() / ((double) size));
            response.put("posts", posts);
            response.put("currentPage", page);
            response.put("totalItems", allPublished.size());
            response.put("totalPages", totalPages);
            return response;
        } catch (Exception e){
            return null;
        }

    }
//
//    public Map<String, Object> findPublishedPostsByNumberOfReplies(int page, int size) {
//        try {
//            List<Post> posts = new ArrayList<Post>();
//            Pageable paging = PageRequest.of(page, size);
//
//            Page<Post> pageTuts = postRepository.findPostsByStatusIsOrderByDateCreatedDesc(PostStatus.Published, paging);
//
//            posts = pageTuts.getContent();
//
//            Map<String, Object> response = new HashMap<>();
//            response.put("posts", posts);
//            response.put("currentPage", pageTuts.getNumber());
//            response.put("totalItems", pageTuts.getTotalElements());
//            response.put("totalPages", pageTuts.getTotalPages());
//
//            return response;
//        } catch (Exception e) {
//            return null;
//        }
//    }

    public void createNewPost(CreatePostRequest request, int userId) {
        PostStatus status = request.getStatus();
        if (status != PostStatus.Published && status != PostStatus.Unpublished)
            throw new StatusNotCorrectException("Status is not correct, try again");

        Date currTime = new Date();
        Post newPost = Post.builder().userId(userId)
                .title(request.getTitle())
                .content(request.getContent())
                .isArchived(false)
                .status(request.getStatus())
                .dateCreated(currTime)
                .dateModified(null)
                .images(request.getImages())
                .attachments(request.getAttachments())
                .postReplies(new ArrayList<>())
                .build();
        postRepository.save(newPost);
    }

    public void updatePost(UpdatePostRequest request, String id, int userId) {
        Post post = postRepository.findPostByPostId(id);
        checkValid(post);
        checkOwnership(post, userId);
        post.setContent(request.getContent());
        post.setTitle(request.getTitle());
        post.setDateModified(new Date());
        postRepository.save(post);
    }

    public List<Post> findAllUnpublishedPosts(int userId) {
        return postRepository.findByStatusAndUserIdOrderByDateCreatedDesc(PostStatus.Unpublished, userId);
    }

    public void updatePostStatus(String id, int userId) {
        Post post = postRepository.findPostByPostId(id);
        checkValid(post);
        checkOwnership(post, userId);
        post.setStatus(PostStatus.Hidden);
        postRepository.save(post);
    }

    public void updateAllowReplyPost(String id, int userId) {
        Post post = postRepository.findPostByPostId(id);
        checkValid(post);
        checkOwnership(post, userId);
        post.setIsArchived(!post.getIsArchived());
        postRepository.save(post);
    }

    public void setPostPublish(String id, int userId, List<GrantedAuthority> authorities) {
        // admin: delete or ban -> published
        // post owner: unpublished / hidden -> published
        Post post = postRepository.findPostByPostId(id);
        if (post.getStatus().equals(PostStatus.Banned)) {
            if (!authorities.contains(new SimpleGrantedAuthority("ban_unban"))) {
                throw new EditNotAllowedException("You are not allowed to change the status of the post");
            }
        }
        if (post.getStatus().equals(PostStatus.Deleted)) {
            if (!authorities.contains(new SimpleGrantedAuthority("delete"))) {
                throw new EditNotAllowedException("You are not allowed to change the status of the post");
            }
        }
        if (post.getStatus().equals(PostStatus.Hidden) || post.getStatus().equals(PostStatus.Unpublished)) {
            checkOwnership(post, userId);
        }
        post.setStatus(PostStatus.Published);
        postRepository.save(post);
    }

    private void checkValid(Post post) {
        if (post == null) throw new PostNotExistException("The post does not exist");
        if (post.getStatus() == PostStatus.Deleted || post.getStatus() == PostStatus.Banned) {
            throw new EditNotAllowedException("Sorry, you are not allowed to edit the post");
        }
    }

    private void checkOwnership(Post post, int userId) {
        if (post.getUserId() != userId)
            throw new EditNotAllowedException("Sorry, you are not allowed to edit the post");
    }

    public void banPost(String id) {
        Post post = postRepository.findPostByPostId(id);
        if (post.getStatus().equals(PostStatus.Banned)) {
            post.setStatus(PostStatus.Published);
        } else if (post.getStatus().equals(PostStatus.Published)) {
            post.setStatus(PostStatus.Banned);
        } else {
            throw new EditNotAllowedException("The post is not published yet");
        }
        postRepository.save(post);
    }
    public void recoverPost(String id) {
        Post post = postRepository.findPostByPostId(id);
        if (post.getStatus().equals(PostStatus.Deleted)) {
            post.setStatus(PostStatus.Published);
        } else {
            throw new EditNotAllowedException("The post is not deleted");
        }
        postRepository.save(post);
    }

    public void deletePost(String id, int userId) {
        Post post = postRepository.findPostByPostId(id);
        if (post == null) throw new PostNotExistException("The post does not exist");
        checkOwnership(post, userId);
        if (post.getStatus() != PostStatus.Deleted) {
            post.setStatus(PostStatus.Deleted);
            postRepository.save(post);
        }
    }

    public List<Post> getHistoryViewPosts(int userId) {
        List<Map<String, Object>> histories = (List<Map<String, Object>>) remotePostHistoryService.getUserAllHistory().getData();

        Gson gson = new Gson();

        List<Post> posts = histories.stream()
                .map(history -> gson.fromJson(gson.toJson(history), HistoryResponse.class))
                .map(history -> postRepository.findPostByPostId(history.getPostId()))
                .filter(post -> post != null && post.getStatus().equals(PostStatus.Published) && post.getUserId() == userId)
                .collect(Collectors.toList());

        return posts;
    }

    public Post getPost(String id) {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        List<GrantedAuthority> authorities = (List<GrantedAuthority>) auth.getAuthorities();
        Post post = this.postRepository.findPostByPostId(id);
        if (post == null) throw new PostNotExistException("The post is not exist");
        PostStatus status = post.getStatus();
        if (status.equals(PostStatus.Hidden) || status.equals(PostStatus.Unpublished)) {
            if (userId != post.getUserId())
                throw new AccessNotAllowedException("You are not allowed to access the post");
        } else if (status.equals(PostStatus.Deleted) || status.equals(PostStatus.Banned)) {
            boolean hasBanUnbanAuthority = authorities.stream()
                    .anyMatch(a -> a.getAuthority().equals("ban_unban"));
            if (userId != post.getUserId() && !hasBanUnbanAuthority)
                throw new AccessNotAllowedException("You are not allowed to access the post");
        }
        return post;
    }


    public List<Post> getUserTop3ReplyPosts() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        return postRepository.findPostByUserId(userId).stream()
                .filter(post -> post.getStatus().equals(PostStatus.Published))
                .sorted((o1, o2) -> {
                    int count1 = 0, count2 = 0;
                    for (PostReply reply : o1.getPostReplies()) {
                        count1++;
                        if (reply.getSubReplies() != null)
                            count1 += reply.getSubReplies().size();
                    }
                    for (PostReply reply : o2.getPostReplies()) {
                        count2++;
                        if (reply.getSubReplies() != null)
                            count2 += reply.getSubReplies().size();
                    }
                    // if counts are the same, order by the dateCreated time descendingly
                    if (count1 == count2) return o2.getDateCreated().compareTo(o1.getDateCreated());
                    return count2 < count1 ? -1 : 1;
                }).limit(3).collect(Collectors.toList());
    }

    public List<Post> getUserPosts() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        return postRepository.findByStatusOrderByDateCreatedDesc(PostStatus.Published).stream()
                .filter(post -> post.getUserId() == userId)
                .collect(Collectors.toList());
    }

    public List<Post> getUserBannedPosts() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        return postRepository.findByStatusOrderByDateCreatedDesc(PostStatus.Banned).stream()
                .filter(post -> post.getUserId() == userId)
                .collect(Collectors.toList());
    }
    public List<Post> getBannedPosts() {
        return postRepository.findByStatusOrderByDateCreatedDesc(PostStatus.Banned);
    }

    public List<Post> getUserHiddenPosts() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        return postRepository.findByStatusOrderByDateCreatedDesc(PostStatus.Hidden).stream()
                .filter(post -> post.getUserId() == userId)
                .collect(Collectors.toList());
    }

    public List<Post> getUserDeletedPosts() {
        LoginUserAuthentication auth = (LoginUserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        int userId = auth.getUserID();
        return postRepository.findByStatusOrderByDateCreatedDesc(PostStatus.Deleted).stream()
                .filter(post -> post.getUserId() == userId)
                .collect(Collectors.toList());
    }

    public PostStatus getAPostStatus(String id) {
        Post post = postRepository.findPostByPostId(id);
        if (post == null) throw new PostNotExistException("The post is not exist");
        return post.getStatus();
    }

    public List<Post> getAllPostsAdmin() {
        return postRepository.findPostByStatusNotIn(Arrays.asList(PostStatus.Hidden, PostStatus.Unpublished));
    }

    public List<Post> getAllDeletedPostsAdmin() {
        return postRepository.findByStatusOrderByDateCreatedDesc(PostStatus.Deleted);
    }



}
