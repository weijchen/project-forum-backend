package com.example.postservice.security;

import feign.RequestInterceptor;
import feign.RequestTemplate;
public class JwtTokenInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        String token = JwtContext.getJwtToken();
        System.out.println("Intercepting: token = " + token);
        template.header("Authorization", token);
    }
}
