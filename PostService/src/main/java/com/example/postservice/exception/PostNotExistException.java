package com.example.postservice.exception;

public class PostNotExistException extends RuntimeException {
    private String message;
    public PostNotExistException(String message) {
        super(message);
        this.message = message;
    }
}