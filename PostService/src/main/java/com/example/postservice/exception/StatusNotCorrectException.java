package com.example.postservice.exception;

public class StatusNotCorrectException extends RuntimeException{
    private String message;
    public StatusNotCorrectException(String message) {
        super(message);
        this.message = message;
    }
}
