package com.example.postservice.exception;

public class ReplyNotExistException extends RuntimeException {
    private String message;
    public ReplyNotExistException(String message) {
        super(message);
        this.message = message;
    }
}
