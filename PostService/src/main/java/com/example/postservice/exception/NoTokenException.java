package com.example.postservice.exception;

public class NoTokenException extends Exception{
    private String message;
    public NoTokenException(String message) {
        super(message);
        this.message = message;
    }
}
