package com.example.postservice.exception;

public class AccessNotAllowedException extends RuntimeException {
    private String message;
    public AccessNotAllowedException(String message) {
        super(message);
        this.message = message;
    }
}
