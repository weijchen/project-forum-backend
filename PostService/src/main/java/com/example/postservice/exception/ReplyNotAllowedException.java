package com.example.postservice.exception;

public class ReplyNotAllowedException extends RuntimeException {
    private String message;

    public ReplyNotAllowedException(String message) {
        super(message);
        this.message = message;
    }
}
