package com.example.postservice.exception;

public class EditNotAllowedException extends RuntimeException {
    private String message;
    public EditNotAllowedException(String message) {
        super(message);
        this.message = message;
    }
}
