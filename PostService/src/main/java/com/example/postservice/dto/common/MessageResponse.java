package com.example.postservice.dto.common;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MessageResponse {
    private Boolean success;
    private String message;
    private Integer statusCode;
}
