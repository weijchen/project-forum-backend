package com.example.postservice.dto.reply;

import com.example.postservice.domain.PostReply;
import com.example.postservice.dto.common.ServiceStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@Builder
@ToString
public class AllReplyResponse {
    ServiceStatus serviceStatus;
    List<PostReply> replyList;
}
