package com.example.postservice.dto.reply;

import lombok.*;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateReplyRequest {
    private String comment;
}
