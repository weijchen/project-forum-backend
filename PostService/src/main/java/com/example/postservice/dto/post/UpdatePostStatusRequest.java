package com.example.postservice.dto.post;

import com.example.postservice.domain.PostStatus;
import lombok.*;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdatePostStatusRequest {
    private PostStatus status;
}
