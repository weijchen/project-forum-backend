package com.example.postservice.dto.post;

import com.example.postservice.domain.PostStatus;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatePostRequest {
    private String title;
    private String content;
    private PostStatus status;
    private List<String> images;
    private List<String> attachments;
}
