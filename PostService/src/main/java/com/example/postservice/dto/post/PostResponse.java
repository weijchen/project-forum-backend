package com.example.postservice.dto.post;

import com.example.postservice.domain.Post;
import com.example.postservice.dto.common.ServiceStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@Builder
@ToString
public class PostResponse {
    ServiceStatus serviceStatus;
    List<Post> postList;
}
