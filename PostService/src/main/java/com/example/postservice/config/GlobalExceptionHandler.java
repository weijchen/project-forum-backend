package com.example.postservice.config;

import com.example.postservice.dto.common.ErrorResponse;
import com.example.postservice.dto.common.MessageResponse;
import com.example.postservice.exception.*;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = {EditNotAllowedException.class})
    public ResponseEntity<MessageResponse> editNotAllowedException(EditNotAllowedException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {PostNotExistException.class})
    public ResponseEntity<MessageResponse> postNotExistException(PostNotExistException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {AccessNotAllowedException.class})
    public ResponseEntity<MessageResponse> accessNotAllowedException(AccessNotAllowedException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {StatusNotCorrectException.class})
    public ResponseEntity<MessageResponse> statusNotCorrectException(StatusNotCorrectException ex) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {NoTokenException.class})
    public ResponseEntity<MessageResponse> noTokenException(NoTokenException e) {
        return new ResponseEntity<>(MessageResponse.builder()
                .message(e.getMessage())
                .success(false)
                .statusCode(HttpStatus.NOT_FOUND.value()).build(), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(FileSizeLimitExceededException.class)
    public ResponseEntity<MessageResponse>  handleFileSizeLimitExceeded(FileSizeLimitExceededException e) {
        return new ResponseEntity<>(MessageResponse.builder()
                .message(e.getMessage())
                .success(false)
                .statusCode(HttpStatus.NOT_FOUND.value()).build(), HttpStatus.BAD_REQUEST);
    }
}
