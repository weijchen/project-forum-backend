package com.example.postservice.config;
//import com.example.postservice.security.JwtAuthenticationEntryPoint;
import com.example.postservice.security.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private JwtFilter jwtFilter;

//    @Autowired
//    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    public void setJwtFilter(JwtFilter jwtFilter) {
        this.jwtFilter = jwtFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .csrf().disable()
                .addFilterAfter(jwtFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.DELETE, "/posts/*/replies/*/sub-replies/*").hasAuthority("delete")
                .antMatchers(HttpMethod.DELETE, "/posts/*/replies").hasAuthority("delete")
                .antMatchers(HttpMethod.POST, "/posts/*/replies").hasAuthority("write")
                .antMatchers("/**" ).hasAuthority("read")
                .anyRequest()
                .authenticated();
    }
}
