package groupone.authservice.aop;

import groupone.authservice.dto.common.MessageResponse;
import groupone.authservice.exception.UserServerException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AOPExceptionHandler {
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<MessageResponse> handleException(Exception e) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(e.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }

    @ExceptionHandler(value = {UserServerException.class})
    public ResponseEntity<MessageResponse> handleUserServerException(Exception e) {
        return new ResponseEntity<>(MessageResponse.builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(e.getMessage())
                .success(false)
                .build(), HttpStatus.OK);
    }
}
