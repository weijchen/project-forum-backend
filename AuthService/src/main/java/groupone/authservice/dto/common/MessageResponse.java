package groupone.authservice.dto.common;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MessageResponse {
    private int statusCode;
    private String message;
    private Boolean success;
}
