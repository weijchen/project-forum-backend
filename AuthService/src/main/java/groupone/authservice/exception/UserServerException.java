package groupone.authservice.exception;

public class UserServerException extends RuntimeException {
    private String message;
    public UserServerException(String message) {
        super(message);
        this.message = message;
    }
}
