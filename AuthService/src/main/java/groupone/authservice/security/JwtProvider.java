package groupone.authservice.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource("classpath:application.properties")
public class JwtProvider {
    @Value("${security.jwt.token.key}")
    private String key;

    public String createToken(AuthUserDetail userDetails) {
        Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
        List<String> authorities = new ArrayList<>();
        for (GrantedAuthority c : userDetails.getAuthorities()) {
            authorities.add(c.getAuthority());
        }
        claims.put("permissions", authorities);
        claims.put("userId", userDetails.getUserId());
        claims.put("role", userDetails.getUserType());
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();
    }
}
