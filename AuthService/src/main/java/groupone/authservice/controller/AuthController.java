package groupone.authservice.controller;

import groupone.authservice.dto.request.LoginRequest;
import groupone.authservice.dto.request.RegisterRequest;
import groupone.authservice.dto.response.DataResponse;
import groupone.authservice.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class AuthController {

    private AuthService authService;

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("auth/login")
    public ResponseEntity<DataResponse> login(@RequestBody LoginRequest request) {
        return authService.login(request);

    }

    @PostMapping("auth/register")
    public ResponseEntity<DataResponse> register(@RequestBody RegisterRequest request) {
        return authService.register(request);
    }
}
