package groupone.authservice.service;

import groupone.authservice.dto.request.LoginRequest;
import groupone.authservice.dto.request.RegisterRequest;
import groupone.authservice.dto.response.DataResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;


@FeignClient(name = "user-service")
public interface RemoteUserService {

    @PostMapping("user-service/login")
    ResponseEntity<DataResponse> login(LoginRequest request);
    @PostMapping("user-service/register")
    ResponseEntity<DataResponse> register(RegisterRequest request);
}

