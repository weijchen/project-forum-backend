package com.example.emailservice.dto.request;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationRequest implements Serializable {
    private String recipient;
    private String msgBody;
    private String subject;
}

