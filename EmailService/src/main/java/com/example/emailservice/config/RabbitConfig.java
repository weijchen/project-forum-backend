package com.example.emailservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    //
    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    Queue emailQueue() {
        return QueueBuilder.durable("q.send-email").build();
    }

    @Bean
    MessageListenerContainer messageListenerContainer() {
        SimpleMessageListenerContainer messageListenerContainer = new SimpleMessageListenerContainer();
        messageListenerContainer.setConnectionFactory(connectionFactory());
        messageListenerContainer.setQueues(emailQueue());
        messageListenerContainer.setMessageListener(new RabbitListener());
        return messageListenerContainer;
    }

    @Bean
    public Queue createUserRegistrationQueue() {
        return QueueBuilder.durable("q.user-registration")
                .withArgument("x-dead-letter-exchange", "x.registration-failure")
                .withArgument("x-dead-letter-routing-key", "fall-back")
                .build();
    }

    @Bean
    public Declarables createPostRegistrationSchema() {
        return new Declarables(
                new DirectExchange("x.user-registration"),
                new Queue("q.send-email"),
                new Binding("q.send-email", Binding.DestinationType.QUEUE, "x.user-registration", "send-email", null)
        );
    }
}
