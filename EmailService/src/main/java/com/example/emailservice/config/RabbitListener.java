package com.example.emailservice.config;

import com.example.emailservice.entity.SimpleMessage;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

public class RabbitListener implements MessageListener {

    @Override
    public void onMessage(Message message) {

        System.out.println("New message received from "
                + message.getMessageProperties().getConsumerQueue()
                + ": "
                + new String(message.getBody()));

        String json = new String(message.getBody());

        ObjectMapper objectMapper = new ObjectMapper();
        SimpleMessage simpleMessage = SimpleMessage.builder().build();

        try {
            JsonNode node = objectMapper.readTree(json);
            String recipient = node.path("recipient").asText();
            String msgBody = node.path("msgBody").asText();
            String subject = node.path("subject").asText();
            simpleMessage.setRecipient(recipient);
            simpleMessage.setMsgBody(msgBody);
            simpleMessage.setSubject(subject);


            JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
            javaMailSender.setHost("smtp.gmail.com");
            javaMailSender.setPort(587);
            javaMailSender.setUsername("forumproject@gmail.com");
            javaMailSender.setPassword("pnjkwpwrdvulhmhf");
            Properties props = javaMailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("forumproject@gmail.com");
            mailMessage.setTo(simpleMessage.getRecipient());
            mailMessage.setText(simpleMessage.getMsgBody());
            mailMessage.setSubject(simpleMessage.getSubject());

            javaMailSender.send(mailMessage);
            System.out.println("sent");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
